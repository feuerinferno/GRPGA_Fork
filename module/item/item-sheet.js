/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class baseItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "item"],
      width: 450,
      height: 450,
    });
  }

  /** @override */
  get template() {
    const path = "systems/grpga/templates/item";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.hbs`;
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.hbs`.

    return `${path}/${this.item.type.toLowerCase()}-sheet.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    // Retrieve base data structure.
    const data = super.getData(options);

    // Grab the item's data.
    const itemData = data.data;

    // Re-define the template data references.
    data.item = itemData;
    data.data = itemData.data;

    data.config = CONFIG.grpga;
    data.mode = this.actor?.data.data.mode || data.config.ruleset;
    data.userGroupSort = this.actor?.data.data.userGroupSort || true;
    data.modeSpecificText = {
      mode: data.mode,
      check: game.i18n.localize(`grpga.item.rollable.${data.mode}.check`),
      skill: game.i18n.localize(`grpga.item.rollable.${data.mode}.skill`),
      spell: game.i18n.localize(`grpga.item.rollable.${data.mode}.spell`),
      technique: game.i18n.localize(`grpga.item.rollable.${data.mode}.technique`),
      rms: game.i18n.localize(`grpga.item.rollable.${data.mode}.rms`),
      advantage: game.i18n.localize(`grpga.item.trait.${data.mode}.advantage`),
      disadvantage: game.i18n.localize(`grpga.item.trait.${data.mode}.disadvantage`),
      perk: game.i18n.localize(`grpga.item.trait.${data.mode}.perk`),
      quirk: game.i18n.localize(`grpga.item.trait.${data.mode}.quirk`),
      accuracy: game.i18n.localize(`grpga.item.ranged-attack.${data.mode}.accuracy`),
      attr: game.i18n.localize(`grpga.item.primary-attribute.${data.mode}.attr`),
      defence: game.i18n.localize(`grpga.item.modifier.${data.mode}.defence`),
      reaction: game.i18n.localize(`grpga.item.modifier.${data.mode}.reaction`),
      targets: game.i18n.localize(`grpga.item.modifier.${data.mode}.targets`),
      formula: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.formula`),
      armourDiv: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.armourDiv`),
      minST: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.minST`),
      damage: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.damage`),
      damageType: game.i18n.localize(`grpga.item.melee-attack.${data.mode}.damageType`),
      dodge: game.i18n.localize(`grpga.item.defence.${data.mode}.dodge`),
      block: game.i18n.localize(`grpga.item.defence.${data.mode}.block`),
      parry: game.i18n.localize(`grpga.item.defence.${data.mode}.parry`)
    }
    return data;
  }

  /* -------------------------------------------- */

  /** @override
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 300;
    sheetBody.css("height", bodyHeight);
    return position;
  }
   */
}
export class poolItemSheet extends baseItemSheet {

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.pool').change(this._onInputChange.bind(this));
  }

  /**
   * Handle the Item Input Change Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onInputChange(event) { // only for pools as they must update tokens
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onInputChange()\n", event);

    const target = event.currentTarget;
    // get the name of the changed element
    const dataname = target.name.split(".")[1];
    // get the new value
    let value = target.value;
    // is this the value attribute, isBar is true
    let isBar = (dataname == "value");

    return this.actor.modifyTokenAttribute(
      `tracked.${this.item.data.data.abbr.toLowerCase()}${isBar ? '' : `.${dataname}`}`,
      Number(value),
      false, isBar
    );
  }
}

export class modifierItemSheet extends baseItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "item", "modifier"],
      width: 600,
      height: 400,
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.modentry-create').click(this._onModEntryCreate.bind(this));
    html.find('.modentry-delete').click(this._onModEntryDelete.bind(this));
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = /[@|#]([\w.\-]+)/gi;
    return formula.replace(dataRgx, (match, term) => {
      let value = getProperty(this.actor.data.data.dynamic, `${term}.value`);
      return (value) ? String(value).trim() : "0";
    });
  }

  /**
   * Handle the Modifier Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryCreate(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onModEntryCreate()\n", event);
    let itemdata = this.item.data.data;

    // no formula, must be accident
    if (itemdata.entries[0].formula.trim() == "") return;
    let modentries = Object.values(itemdata.entries);
    modentries.push(
      {
        value: this.actor ? Math.round(eval(this._replaceData(modentries[0].formula).replace(/[^-()\d/*+.]/g, ''))) : 0,
        formula: modentries[0].formula,
        category: modentries[0].category,
        targets: modentries[0].targets.trim()
      }
    );
    modentries[0] = { value: 0, formula: "", category: "", targets: "" };
    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Modifier Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryDelete(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onModEntryDelete()\n", event);
    let itemdata = this.item.data.data;
    let element = event.currentTarget;
    let index = element.closest(".modentry").dataset.id;
    delete itemdata.entries[index];
    let modentries = Object.values(itemdata.entries);

    let data = {
      chartype: itemdata.chartype,
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'data': data });
  }
}

export class variableItemSheet extends baseItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "item", "variable"],
      width: 300,
      height: 400,
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.varentry-create').click(this._onVarEntryCreate.bind(this));
    html.find('.varentry-delete').click(this._onVarEntryDelete.bind(this));
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = new RegExp(/@([a-z.0-9_\-]+)/gi);
    return formula.replace(dataRgx, (match, term) => {
      let value = getProperty(this.actor.data.data.dynamic, `${term}.value`);
      return (value) ? String(value).trim() : "0";
    });
  }

  /**
   * Handle the Variable Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryCreate(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onVarEntryCreate()\n", event);
    let itemdata = this.item.data.data;

    // no formula, either accident or request for recalc
    let varentries = Object.values(itemdata.entries);
    if (itemdata.entries[0].formula.trim() == "") {
      for (let entry of varentries) {
        entry.value = Math.round(eval(this._replaceData(entry.formula).replace(/[^-()\d/*+.]/g, '')));
      }
    } else {
      varentries.push(
        {
          value: this.actor ? Math.round(eval(this._replaceData(varentries[0].formula).replace(/[^-()\d/*+.]/g, ''))) : 0,
          formula: varentries[0].formula,
          label: varentries[0].label.trim()
        }
      );
    }
    varentries[0] = { value: 0, formula: "", label: "" };
    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'data': data });
  }

  /**
   * Handle the Variable Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryDelete(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onVarEntryDelete()\n", event);
    let itemdata = this.item.data.data;
    let element = event.currentTarget;
    let index = element.closest(".varentry").dataset.id;
    delete itemdata.entries[index];
    let varentries = Object.values(itemdata.entries);

    let data = {
      chartype: itemdata.chartype,
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'data': data });
  }
}
