/**
 * Perform a system migration for the entire World, applying migrations for Actors, Items, and Compendium packs
 * @return {Promise}      A Promise which resolves once the migration is completed
 */
export const migrateWorld = async function () {
  ui.notifications.notify(`Beginning Migration to grpga ${game.system.data.version}`, { permanent: true });

  // Migrate World Actors
  for (let a of game.actors.contents) {
    try {
      const updateData = migrateActorData(a.data);
      if (!foundry.utils.isObjectEmpty(updateData)) {
        console.debug(`Migrating Actor ${a.name}`);
        await a.update(updateData, { enforceTypes: false });
      }
    } catch (err) {
      err.message = `Failed migration for Actor ${a.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate World Items
  for (let i of game.items.contents) {
    try {
      const updateData = migrateItemData(i.toObject());
      if (!foundry.utils.isObjectEmpty(updateData)) {
        console.debug(`Migrating Item ${i.name}`);
        await i.update(updateData, { enforceTypes: false });
      }
    } catch (err) {
      err.message = `Failed migration for Item ${i.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate Actor Override Tokens
  for (let s of game.scenes.contents) {
    try {
      const updateData = migrateSceneData(s.data);
      if (!foundry.utils.isObjectEmpty(updateData)) {
        console.debug(`Migrating Scene entity ${s.name}`);
        await s.update(updateData, { enforceTypes: false });
        // If we do not do this, then synthetic token actors remain in cache
        // with the un-updated actorData.
        s.tokens.contents.forEach(t => t._actor = null);
      }
    } catch (err) {
      err.message = `Failed migration for Scene ${s.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate World Compendium Packs
  for (let p of game.packs) {
    if (p.metadata.package !== "world") continue; // only commented out to migrate system packs
    if (!["Actor", "Item", "Scene"].includes(p.metadata.entity)) continue;
    await migrateCompendium(p);
  }

  game.settings.set("grpga", "systemMigrationVersion", game.system.data.version);
  ui.notifications.notify(`Migration to GRPGA ${game.system.data.version} Finished`, { permanent: true });
}

/**
 * Apply migration rules to all Documents within a single Compendium pack
 * @param pack
 * @return {Promise}
 */
export const migrateCompendium = async function (pack) {
  const entity = pack.metadata.entity;
  if (!["Actor", "Item", "Scene"].includes(entity)) return;

  // Unlock the pack for editing
  const wasLocked = pack.locked;
  await pack.configure({ locked: false });

  // Begin by requesting server-side data model migration and get the migrated content
  await pack.migrate();
  const documents = await pack.getDocuments();

  // Iterate over compendium entries - applying fine-tuned migration functions
  for (let doc of documents) {
    let updateData = {};
    try {
      switch (entity) {
        case "Actor":
          updateData = migrateActorData(doc.data);
          break;
        case "Item":
          updateData = migrateItemData(doc.toObject());
          break;
        case "Scene":
          updateData = migrateSceneData(doc.data);
          break;
      }

      // Save the entry, if data was changed
      if (foundry.utils.isObjectEmpty(updateData)) continue;
      await doc.update(updateData);
      console.debug(`Migrated ${entity} entity ${doc.name} in Compendium ${pack.collection}`);
    }
    catch (err) {    // Handle migration failures
      err.message = `Failed migration for entity ${doc.name} in pack ${pack.collection}: ${err.message}`;
      console.error(err);
    }
  }

  // Apply the original locked status for the pack
  await pack.configure({ locked: wasLocked });
  console.debug(`Migrated all ${entity} entities from Compendium ${pack.collection}`);
};

/**
 * Migrate a single Actor entity to incorporate latest data model changes
 * Return an Object of updateData to be applied
 * @param {object} actor    The actor data object to update
 * @return {Object}         The updateData to apply
 */
export const migrateActorData = function (actor) {
  const updateData = {};

  // Actor Data Updates go here
  if (actor.data) {
    // _migrateActorMovement(actor, updateData);
    // _migrateActorSenses(actor, updateData);
    // _migrateActorType(actor, updateData);
  }

  // Migrate Owned Items
  if (!actor.items) return updateData;
  const items = actor.items.reduce((arr, i) => {
    // Migrate the Owned Item
    const itemData = i instanceof CONFIG.Item.documentClass ? i.toObject() : i;
    let itemUpdate = migrateItemData(itemData);

    // Update the Owned Item
    if (!isObjectEmpty(itemUpdate)) {
      itemUpdate._id = itemData._id;
      arr.push(expandObject(itemUpdate));
    }

    return arr;
  }, []);
  if (items.length > 0) updateData.items = items;
  return updateData;
}

/**
 * Migrate a single Item entity to incorporate latest data model changes
 *
 * @param {object} item  Item data to migrate
 * @return {object}      The updateData to apply
 */
export const migrateItemData = function (item) {
  const updateData = {};
  _migrateItemGrouping(item, updateData);
  return updateData;
}

/**
 * Migrate a single Scene entity to incorporate changes to the data model of its actor data overrides
 * Return an Object of updateData to be applied
 * @param {Object} scene  The Scene data to Update
 * @return {Object}       The updateData to apply
 */
export const migrateSceneData = function (scene) {
  const tokens = scene.tokens.map(token => {
    const t = token.toJSON();
    if (!t.actorId || t.actorLink) {
      t.actorData = {};
    }
    else if (!game.actors.has(t.actorId)) {
      t.actorId = null;
      t.actorData = {};
    }
    else if (!t.actorLink) {
      const actorData = duplicate(t.actorData);
      actorData.type = token.actor?.type;
      const update = migrateActorData(actorData);
      ['items', 'effects'].forEach(embeddedName => {
        if (!update[embeddedName]?.length) return;
        const updates = new Map(update[embeddedName].map(u => [u._id, u]));
        t.actorData[embeddedName].forEach(original => {
          const update = updates.get(original._id);
          if (update) mergeObject(original, update);
        });
        delete update[embeddedName];
      });

      mergeObject(t.actorData, update);
    }
    return t;
  });
  return { tokens };
};

/**
 * Set the group field for each item in accordance with existing settings
 *
 * @param {object} item        Item data to migrate
 * @param {object} updateData  Existing update to expand upon
 * @return {object}            The updateData to apply
 * @private
 */
function _migrateItemGrouping(item, updateData) {

  if (item.data.group != "") return updateData
  switch (item.type) {
    case "Trait":
    case "Defence":
    case "Rollable": {
      updateData["data.group"] = item.data.category;
      break;
    }
    default: {
      updateData["data.group"] = item.type;
      break;
    }
  }
  return updateData;
}


export class Refresh {

  static async refreshWorld() {
    ui.notifications.notify(`Beginning System Refresh`);
    for (let i of game.items.contents) {
      await this.refreshItemData(i, duplicate(i.data), null);
    }

    for (let a of game.actors.contents) {
      if (CONFIG.grpga.testMode) console.debug(a.name);
      a.update(await this.refreshActorData(a));
    }

    for (let p of game.packs) {
      if (p.metadata.entity == "Item" && p.metadata.package == "world")
        p.getContent().then(async (items) => {
          items.forEach(async (i) => {
            await this.refreshItemData(i, duplicate(i.data), null);
          })
        })

      if (p.metadata.entity == "Actor" && p.metadata.package == "world") {
        p.getContent().then(async (actors) => {
          actors.forEach(async (a) => {
            p.updateEntity(await this.refreshActorData(a))
          })
        })
      }
    }
    ui.notifications.notify(`Refresh of GRPGA Finished`);
  }

  static async refreshActorData(actor) {
    for (let i of actor.items.contents) {
      i.data.data.chartype = actor.data.type;
      await this.refreshItemData(i, duplicate(i.data), actor);
    }
    return actor.data
  }

  static async refreshItemData(item, itemData, actor) {
    // if an item has become misaligned from its ruleset, realign it
    if (actor) {
      if (itemData.data.chartype != actor.data.type) {
        console.error(`Fixing ${itemData.data.chartype} in ${actor.name}`);
        itemData.data.chartype = actor.data.type;
      }
    } else {
      if (itemData.data.chartype != CONFIG.grpga.chartype) {
        console.error(`Fixing ${itemData.data.chartype}`);
        itemData.data.chartype = CONFIG.grpga.chartype;
      }
    }

    // check each entry in a Variable to make sure that the value matches the formula.
    switch (itemData.type) {
      case "Variable": {
        let entries = Object.values(itemData.data.entries);
        for (let i = 1; i < entries.length; i++) {
          if (entries[i].value != Number(entries[i].formula)) {
            if (Number.isNumeric(entries[i].formula)) {
              console.debug(`${entries[i].value} being corrected to ${entries[i].formula} in ${entries[i].label} of ${itemData.name} in ${actor?.name}`);
              entries[i].value = Number(entries[i].formula);
            }
          }
        }
      }
    }
    // I introduced a forceUpdate variable into most items using
    // itemData.data.forceUpdate = !itemData.data.forceUpdate;
    // do not need it for now but unwrap it and everything gets updated.
    return await item.update(itemData);
  }
}

/**
 * Ready hook loads tables, and override's foundry's entity link functions to provide extension to pseudo entities
 */
Hooks.once("ready", function () {

  if (CONFIG.grpga.testMode) console.debug("Starting Ready");

  // Determine whether a system migration is required and feasible
  if (!game.user.isGM) return;
  const currentVersion = game.settings.get("grpga", "systemMigrationVersion");
  const NEEDS_MIGRATION_VERSION = 0.91;
  const COMPATIBLE_MIGRATION_VERSION = 0.80;
  const totalDocuments = game.actors.size + game.scenes.size + game.items.size;
  if (!currentVersion && totalDocuments === 0) return game.settings.set("grpga", "systemMigrationVersion", game.system.data.version);
  const needsMigration = !currentVersion || isNewerVersion(NEEDS_MIGRATION_VERSION, currentVersion);
  if (!needsMigration) return;

  // Perform the migration
  if (currentVersion && isNewerVersion(COMPATIBLE_MIGRATION_VERSION, currentVersion)) {
    const warning = `Your system data is from too old a Foundry version and cannot be reliably migrated to the latest version. The process will be attempted, but errors may occur.`;
    ui.notifications.error(warning, { permanent: true });
  }
  return migrateWorld();

  let needRefresh = game.settings.get("grpga", "systemRefreshOnReady");

  if (needRefresh && game.user.isGM) {
    new Dialog({
      title: "System Refresh",
      content: `<p style="color:#000;">You have chosen to be able to refresh the system when restarting.<br>
      This process will:<br>
      - Recalculate the entries of Variables and Modifiers<br>
      - Confirm that each item has the same chartype as its actor.</p>`,
      buttons: {
        refresh: {
          label: "Refresh",
          callback: html => Refresh.refreshWorld()
        },
        skip: {
          label: "Skip",
          callback: html => { }
        }
      }
    }).render(true);
    return;
  }
});
