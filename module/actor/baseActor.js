/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class baseActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareDerivedData() {

    const actorData = this.data;

    actorData.data.useFAHR = game.settings.get("grpga", "useFAHR");
    actorData.data.useD6Pool = game.settings.get("grpga", "useD6Pool");
    actorData.data.rankMode = game.settings.get("grpga", "rankMode");
    actorData.data.autoCrit = game.settings.get("grpga", "autoCriticalRolls");
    actorData.data.userGroupSort = game.settings.get("grpga", "userGroupSort");

    if (CONFIG.grpga.testMode) console.debug("entering prepareDerivedData()\n", [this, actorData]);

    let isD20 = false, is3D6 = false, isOaTS = false, isD100 = false, isVsD = false;

    const actoritems = actorData.items;
    // for counting the ranks stored in Variable Items for some systems
    let actorranks = {};
    let rankitems = [];
    let hasranks = false;

    const myData = actorData.data;
    myData.defence = {};
    myData.dynamic = {};
    myData.primaries = {};
    myData.modifiers = {};
    myData.tracked = {};

    // group the items for efficient iteration
    const actormods = [];
    const actornonmods = [];
    const actorrollables = [];

    switch (actorData.type) {
      case "CharacterD20":
      case "CharacterD20Custom":{
        isD20 = true;
        myData.bm.quarter = myData.bm.step * 2;
        myData.bm.half = myData.bm.step * 3;
        myData.bm.move = myData.bm.step * 4;
        myData.mode = "d20"; // used to effect system-specific localisation
        break;
      }
      case "Character3D6": {
        is3D6 = true;
        myData.mode = "3d6"; // same as above
        break;
      }
      case "CharacterOaTS": {
        isOaTS = true;
        myData.mode = "oats"; // same as above
        break;
      }
      case "CharacterD100": {
        isD100 = true;
        myData.bm.quarter = Number(myData.bm.step * 1.5);
        myData.bm.half = myData.bm.step * 2;
        myData.bm.move = myData.bm.step * 3;
        myData.bm.sprint = myData.bm.step * 4;
        myData.bm.dash = myData.bm.step * 5;
        myData.dynamic.woundsbleed = { "value": 0 };
        myData.dynamic.woundspen = { "value": 0 };
        myData.mode = "d100"; // same as above
        break;
      }
      case "CharacterVsD": {
        isVsD = true;
        hasranks = true;
        myData.dynamic.woundsbleed = { "value": 0 };
        myData.dynamic.woundspen = { "value": 0 };
        myData.mode = "vsd"; // same as above
        myData.dynamic.visionType = "normal";
        break;
      }
    }

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];


    for (const item of actoritems) {
      const itemdata = item.data;
      // collect non-modifier items with formula
      if (itemdata.data.formula) actornonmods.push(itemdata);

      switch (itemdata.type) {
        case "Pool": {
          let itemID = itemdata.data.abbr.toLowerCase();
          // write the pool to tracked so it can be seen by the token
          myData.tracked[itemID] = {
            _id: itemdata._id,
            abbr: itemdata.data.abbr,
            name: itemdata.name,
            state: itemdata.data.state,
            min: Number(itemdata.data.min),
            value: Number(itemdata.data.value),
            max: Number(itemdata.data.max),
            notes: itemdata.data.notes,
            type: itemdata.type
          };
          myData.dynamic[itemID] = {
            name: itemdata.name,
            abbr: itemdata.data.abbr,
            value: Number(itemdata.data.value),
            max: Number(itemdata.data.max),
            _id: itemdata._id,
            notes: itemdata.data.notes,
          };
          // add the term to the layer
          if (itemID) nextLayer.push(itemID);
          break;
        }
        case "Modifier": {
          let itemID = this.slugify(itemdata.name);
          myData.modifiers[itemID] = itemdata;
          actormods.push(itemdata);
          itemdata.data.tempName = itemdata.name;
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack": {
          // write these items to dynamic
          let itemID = this.slugify(itemdata.name);
          myData.dynamic[itemID] = {
            name: itemdata.name,
            value: Number(itemdata.data.value),
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };
          if (Number.isNumeric(itemdata.data.formula)) nextLayer.push(itemID);
          actorrollables.push(itemdata);
          break;
        }
        case "Defence":
        case "Rollable": {
          // write these items to dynamic
          let itemID = this.slugify(itemdata.name);
          myData.dynamic[itemID] = {
            name: itemdata.name,
            formula: itemdata.data.formula,
            value: Number(itemdata.data.value),
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };

          // set an actorranks value of 0 for each Rollable and Defence
          actorranks[itemID] = 0;

          if (Number.isNumeric(itemdata.data.formula) || itemdata.data.formula.startsWith("#")) nextLayer.push(itemID);
          actorrollables.push(itemdata);
          break;
        }
        case "Hit-Location": {
          switch (myData.mode) {
            case "vsd":
            case "d100": {
              // summary of wound effects
              myData.dynamic.woundsbleed.value += itemdata.data.damageResistance;
              myData.dynamic.woundspen.value += itemdata.data.damage;
            }
          }
          break;
        }
        case "Variable": {
          // write these items to dynamic
          let itemID = this.slugify(itemdata.name);
          myData.dynamic[itemID] = {
            name: itemdata.name,
            value: Number(itemdata.data.value),
            formula: itemdata.data.formula,
            label: itemdata.data.label,
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };
          // add the term to the layer if the formula is a number
          if (Number.isNumeric(itemdata.data.formula)) nextLayer.push(itemID);

          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (itemdata.name.startsWith("Ranks:")) rankitems.push(itemdata);

          break;
        }
        case "Primary-Attribute": {
          itemdata.data.tempattr = Number(itemdata.data.attr);
          itemdata.data.value = (isD20 || isOaTS) ? Math.floor((Number(itemdata.data.tempattr) - 10) / 2) : Number(itemdata.data.tempattr);

          // write name and value of the item to dynamic
          let itemID = this.slugify(itemdata.data.abbr);
          myData.dynamic[itemID] = {
            name: itemdata.data.abbr,
            tempattr: itemdata.data.tempattr,
            value: itemdata.data.value,
            _id: itemdata._id,
            notes: itemdata.data.notes,
            type: itemdata.type
          };

          // write the item reference to primaries so you may modifiy it later
          myData.primaries[itemID] = itemdata;

          // add the term to the layer
          if (itemID) nextLayer.push(itemID);
          break;
        }
        default: { // Traits that define a vision type for the actor
          switch (itemdata.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              myData.dynamic.visionType = itemdata.name;
            }
          }
        }
      }
    }

    if (hasranks) {
      if (CONFIG.grpga.testMode) console.debug("Rankitems to be polled:\n", [this, rankitems]);

      // poll the rank items for systems which count them
      for (const itemdata of rankitems) {
        for (let entry of Object.values(itemdata.data.entries)) {
          let target = this.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
          }
        }
      }

      if (CONFIG.grpga.testMode) console.debug("Rankdata after polling:\n", [this, actorranks]);

      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorranks)) {
        if (myData.dynamic[name]) {

          // create a name-ranks entry in dynamic so its value can be used as a formula reference
          myData.dynamic[`${name}-ranks`] = {
            name: `${myData.dynamic[name].name}-Ranks`,
            value: ranks
          };

          // calculate rank bonus to assign to value and moddedvalue
          let value = this.rankCalculator(ranks);
          myData.dynamic[name].formula = `#${ranks}`;
          myData.dynamic[name].ranks = ranks;
          myData.dynamic[name].value = value;
          myData.dynamic[name].moddedvalue = value;

          if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were assigned to : ${name}`);
        } else {
          if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
    }

    // if the formula is a number, process it now
    switch (myData.mode) {
      case "vsd": {
        for (const itemdata of actornonmods) {
          let dynID = this.slugify(itemdata.name);
          switch (itemdata.type) {
            case "Defence":
            case "Rollable": {
              // write to Actor Item???
              itemdata.data.formula = myData.dynamic[dynID].formula;
              itemdata.data.value = myData.dynamic[dynID].value;
              break;
            }
            default: {
              if (Number.isNumeric(itemdata.data.formula)) {
                // write to Actor Item
                itemdata.data.value = Number(itemdata.data.formula);
                // write to dynamic
                myData.dynamic[dynID].value = itemdata.data.value;
              }
            }
          }
        }
        break;
      }
      default: {
        for (const itemdata of actornonmods) {
          if (Number.isNumeric(itemdata.data.formula)) {
            let dynID = this.slugify(itemdata.name);
            // write to Actor Item
            itemdata.data.value = Number(itemdata.data.formula);
            // write to dynamic
            myData.dynamic[dynID].value = itemdata.data.value;
          }
        }
      }
    }

    // the modifier has a primod and is in effect
    for (const itemdata of actormods) {
      if (itemdata.data.primary && itemdata.data.inEffect) {
        for (let entry of Object.values(itemdata.data.entries)) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            if (Number.isNumeric(entry.formula)) {
              entry.value = Number(entry.formula);
            } else {
              if (entry.formula == "") continue;
              let formData = this._replaceData(entry.formula);
              try {
                entry.value = Math.round(eval(formData.value.replace(/[^-()\d/*+.Dd]/g, '')));
              } catch (err) {
                console.debug(err);
              }
              if (!itemdata.data.tempName.includes(formData.label)) itemdata.data.tempName += formData.label;
            }
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const target of cases) {
              // we have found the target in dynamic
              if (myData.dynamic[target]) {

                // write to primaries
                let origitem = myData.primaries[target];
                origitem.data.tempattr = Number(origitem.data.tempattr) + entry.value;
                origitem.data.value = (isD20 || isOaTS) ? Math.floor((origitem.data.tempattr - 10) / 2) : origitem.data.tempattr;

                // write to dynamic
                myData.dynamic[target].tempattr = Number(myData.dynamic[target].tempattr) + entry.value;
                myData.dynamic[target].value = (isD20 || isOaTS) ? Math.floor((myData.dynamic[target].tempattr - 10) / 2) : myData.dynamic[target].tempattr;
              }
            }
          }
        }
      }
    }

    // calculate the remaining formulae one layer at a time
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const itemdata of actornonmods) {

        //ignore items with no formula or whose formula is a number which has already been processed
        if (!itemdata.data.formula || Number.isNumeric(itemdata.data.formula) || itemdata.data.formula.startsWith("#")) continue;

        for (const target of previousLayer) {

          // if the name of this item equals the target then get out of the loop
          if (itemdata.name.toLowerCase() == target) continue;
          // if this formula refers to an item in the previous layer then process it
          if (itemdata.data.formula.includes(target)) {

            let dynID = this.slugify(itemdata.name);

            // write to Actor Item
            let formData = this._replaceData(itemdata.data.formula);
            try {
              itemdata.data.value = Math.round(eval(formData.value.replace(/[^-()\d/*+.Dd]/g, '')) * 100) / 100;
              itemdata.data.moddedformula = "" + itemdata.data.value; // store the result as a String
            } catch (err) {
              // store the formula ready to be rolled
              itemdata.data.moddedformula = formData.value;
              itemdata.data.value = 0; // eliminate any old values
              console.debug(err);
            }

            // write to dynamic
            myData.dynamic[dynID].value = itemdata.data.value; // this pushes a new value to the target, not the item

            // put this item into the next layer
            nextLayer.push(dynID);
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const itemdata of actormods) {
      for (let entry of Object.values(itemdata.data.entries)) {
        if (Number.isNumeric(entry.formula)) {
          entry.value = Number(entry.formula);
        } else {
          if (entry.formula == "") continue;
          let formData = this._replaceData(entry.formula);
          try {
            entry.value = Math.round(eval(formData.value.replace(/[^-()\d/*+.Dd]/g, '')));
          } catch (err) {
            console.debug(err);
          }
          if (!itemdata.data.tempName.includes(formData.label)) itemdata.data.tempName += formData.label;
        }
      }
    }

    // calculate the modified value of the item
    for (const itemdata of actorrollables) {
      itemdata.data.moddedvalue = itemdata.data.value;
      // if there is no modified formula, make it the value
      if (itemdata.data.moddedformula == undefined) itemdata.data.moddedformula = "" + itemdata.data.value;
      let itemID = this.slugify(itemdata.name);
      switch (itemdata.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "attack");
          break;
        }
        case "Defence": {
          itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "defence");
          break;
        }
        case "Rollable": {
          switch (itemdata.data.category) {
            case "check": {
              itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "reaction");
              break;
            }
            case "skill": {
              itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "skill");
              break;
            }
            case "spell": {
              itemdata.data.moddedvalue += this.fetchRelevantModifiers(itemdata.name, "spell");
              break;
            }
          }
          myData.dynamic[itemID].category = itemdata.data.category;
          break;
        }
      }
      myData.dynamic[itemID].moddedvalue = itemdata.data.moddedvalue;
    }

    if (isVsD || isD100) {
      myData.bs.value = (myData.dynamic["declared-action"]) ? myData.dynamic["declared-action"].value : myData.bs.value;
      myData.defence = {
        armourType: myData.dynamic["armor-type"]?.formula || "",
        meleeDB: myData.dynamic["melee-db"]?.moddedvalue || 0,
        missileDB: myData.dynamic["missile-db"]?.moddedvalue || 0
      };
    }
  }

  slugify(text) {
    return text
      .toString()                     // Cast to string
      .toLowerCase()                  // Convert the string to lowercase letters
      .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim()                         // Remove whitespace from both sides of a string
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-');        // Replace multiple - with single -
  }

  rankCalculator(ranks) {
    switch (this.data.data.rankMode) {
      case "0521h": {
        if (ranks > 30) {
          return 80 + (ranks - 30) / 2;
        } else if (ranks > 20) {
          return 70 + (ranks - 20);
        } else if (ranks > 10) {
          return 50 + (ranks - 10) * 2;
        } else if (ranks > 0) {
          return ranks * 5;
        } else {
          return -25;
        }
      }
      case "05321": {
        if (ranks > 30) {
          return 100 + (ranks - 30);
        } else if (ranks > 20) {
          return 80 + (ranks - 20) * 2;
        } else if (ranks > 10) {
          return 50 + (ranks - 10) * 3;
        } else if (ranks > 0) {
          return ranks * 5;
        } else {
          return -25;
        }
      }
      case "0321h": {
        if (ranks > 30) {
          return 60 + (ranks - 30) / 2;
        } else if (ranks > 20) {
          return 50 + (ranks - 20);
        } else if (ranks > 10) {
          return 30 + (ranks - 10) * 2;
        } else if (ranks > 0) {
          return ranks * 3;
        } else {
          return -15;
        }
      }
      case "0521":
        if (ranks == 0) return -25;
      case "1521":
        if (ranks == 0) return -15;
      case "521": {
        if (ranks > 20) {
          return 70 + ranks - 20;
        } else if (ranks > 10) {
          return 50 + (ranks - 10) * 2;
        } else {
          return ranks * 5;
        }
      }
      default: { // 111
        return ranks;
      }
    }
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = /[@|#]([\w.\-]+)/gi;
    let tempName = "";
    let findTerms = (match, term) => {
      switch (match[0]) {
        case "@": {
          let value = getProperty(this.data.data.dynamic, `${term}.value`) || getProperty(this.data.data.tracked, `${term}.value`);
          let label = getProperty(this.data.data.dynamic, `${term}.label`);
          tempName += (label) ? ` (${label})` : "";
          return (value) ? String(value).trim() : "0";
        }
        case "#": {
          return this.rankCalculator(Number(term));
        }
      }
    };

    let replyData = formula.replace(dataRgx, findTerms);
    return { value: replyData, label: tempName };
  }

  async setConditions(newValue, attrName) {
    if (CONFIG.grpga.testMode) console.debug("entering setConditions()\n", [newValue, attrName]);

    const tracked = this.data.data.tracked;
    const attr = attrName.split('.')[2];
    const item = this.items.get(tracked[attr]._id);
    const itemData = item.data.data;
    let attrValue = itemData.value;
    let attrMax = itemData.max;
    let attrState = itemData.state;
    let attrMin = itemData.min;

    // Assign the variables
    if (attrName.includes('.max')) {
      attrMax = newValue;
    } else if (attrName.includes('.min')) {
      attrMin = newValue;
    } else {
      attrValue = newValue;
    }
    const ratio = attrValue / attrMax;

    switch (attr) {
      case "vit": { // Star Wars D20
        if (attrValue < 1) attrState = '[EXHSTD]';
        else if (ratio < 0.25) attrState = '[BUSHED]';
        else if (ratio < 0.5) attrState = '[TIRED]';
        else if (ratio < 0.75) attrState = '[WINDED]';
        else attrState = '[FIT]';
        attrMin = 0;
        break;
      }
      case "wnd": { // Star Wars D20
        if (attrValue < -9) attrState = '[DEAD]';
        else if (attrValue < 1) attrState = '[UNC]';
        else if (ratio < 0.25) attrState = '[WND]';
        else if (ratio < 0.5) attrState = '[BLD]';
        else if (ratio < 0.75) attrState = '[BRU]';
        else attrState = '[FIT]';
        attrMin = -10;
        break;
      }
      case "hp": {
        if (this.data.type == "CharacterD20") { // health state for D20 system
          if (attrValue < -9) attrState = '[DEAD]';
          else if (attrValue < 1) attrState = '[UNC]';
          else if (ratio < 0.25) attrState = '[WND]';
          else if (ratio < 0.5) attrState = '[BLD]';
          else if (ratio < 0.75) attrState = '[BRU]';
          else attrState = '[FIT]';
          attrMin = -10;
        } else if (this.data.type == "CharacterVsD") { // health state for Against the Darkmaster
          if (this.data.data.useFAHR) {
            switch (Math.trunc(ratio * 8)) {
              case 1: {
                attrState = '[1/8 BRU]';
                break;
              }
              case 2: {
                attrState = '[1/4 BRU]';
                break;
              }
              case 3: {
                attrState = '[3/8 BRU]';
                break;
              }
              case 4: {
                attrState = '[1/2]';
                break;
              }
              case 5: {
                attrState = '[5/8]';
                break;
              }
              case 6: {
                attrState = '[3/4]';
                break;
              }
              case 7: {
                attrState = '[7/8]';
                break;
              }
              case 8: {
                attrState = '[FIT]';
                break;
              }
              default: { // bad shape
                if (ratio <= -1) {
                  attrState = '[DEAD]';
                } else if (ratio <= -0.5) {
                  attrState = '[DYING]';
                } else if (ratio <= 0) {
                  attrState = '[INC]';
                } else {
                  attrState = '[< 1/8 BRU]';
                }
              }
            }
            attrMin = -attrMax;
          } else {
            if (attrValue < -49) attrState = '[DYING]';
            else if (attrValue < 1) attrState = '[INC]';
            else if (ratio < 0.5) attrState = '[BRU]';
            else attrState = '[FIT]';
            attrMin = -100;
          }
        } else { // health state for 3D6 system
          // set the limits
          switch (Math.trunc(ratio)) {
            case 0: {
              if (ratio <= 0) { // collapse
                attrState = '[C]';
                break;
              } else if (attrValue < (attrMax / 3)) { // reeling
                attrState = '[R]';
                break;
              }
              // healthy, no break
            }
            case 1: { // healthy
              attrState = '[H]';
              break;
            }
            case -1: { // death check at -1
              attrState = '[-X]';
              break;
            }
            case -2: { // death check at -2
              attrState = '[-2X]';
              break;
            }
            case -3: { // death check at -3
              attrState = '[-3X]';
              break;
            }
            case -4: { // death check at -4
              attrState = '[-4X]';
              break;
            }
            default: { // dead
              attrState = '[DEAD]';
              break;
            }
          }
          attrMin = -attrMax * 5;
        }
        break;
      }
      case "fp": {
        // set the limits
        switch (Math.trunc(ratio)) {
          case 0: {
            if (ratio <= 0) { // collapse
              attrState = '[C]';
              break;
            } else if (attrValue < (attrMax / 3)) { // tired
              attrState = '[T]';
              break;
            }
            // fresh, no break
          }
          case 1: { // fresh
            attrState = '[F]';
            break;
          }
          default: { // unconscious
            attrState = '[UNC]';
            break;
          }
        }
        attrMin = -attrMax;
        break;
      }
      default: { // assume that the state is measured in eighths
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = '[1/8]';
            break;
          }
          case 2: {
            attrState = '[1/4]';
            break;
          }
          case 3: {
            attrState = '[3/8]';
            break;
          }
          case 4: {
            attrState = '[1/2]';
            break;
          }
          case 5: {
            attrState = '[5/8]';
            break;
          }
          case 6: {
            attrState = '[3/4]';
            break;
          }
          case 7: {
            attrState = '[7/8]';
            break;
          }
          case 8: {
            attrState = '[Full]';
            break;
          }
          default: { // dead
            if (ratio <= 0) { // empty
              attrState = '[Empty]';
            } else {
              attrState = '[< 1/8]';
            }
          }
        }
      }
    }
    itemData.min = attrMin;
    itemData.value = attrValue;
    itemData.max = attrMax;
    itemData.state = attrState;

    // FIXME: change this so it updates the entire item then updates the tracked value in the actor. (unless that happens automatically)
    return await item.update({ ['data']: itemData });
  }

  // a test method for executing token actions. SysDev part 8 covers a better way to do this.
  attack() {
    ui.notifications.info("You have triggered the attack method of this actor.");
    let attacks = this.data.items.filter(i => (i.type == "Melee-Attack" || i.type == "Ranged-Attack"));

    let attackscript = `<div id="attacks">`;

    for (const item of attacks) {
      const itemdata = item.data;
      attackscript +=
        `<div class="attack" data-item-id=${itemdata._id} title="${itemdata.data.notes}">
        <label class="rollable" data-name="${itemdata.name}" data-roll=${itemdata.data.value} data-type="attack">
          [${itemdata.data.value}] ${itemdata.name}
        </label>
        <label class="rollable" data-name="${itemdata.name}" data-roll="${itemdata.data.damage}" data-damageType="${itemdata.data.damageType}" data-armourdiv=${itemdata.data.armourDiv} data-type="damage">
          ${itemdata.data.damage} ${itemdata.data.damageType}
        </label>
      </div>
    </div>`
    }

    attackscript += `</div>`;

    //html.find('.rollable').click(this.sheet._onRoll.bind(this));

    new Dialog({
      title: "Attacks",
      content: attackscript,
      buttons: {}
    }).render(true);
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * Logic for pools to be edited by plus-minus and text input (on Actor and Item) is also redirected here.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    if (CONFIG.grpga.testMode) console.debug("entering modifyTokenAttribute()\n", [attribute, value, isDelta, isBar]);

    // fetch the pool if the attribute changing is 'value', or the attribute itelf if not
    const current = getProperty(this.data.data, attribute);
    if (isBar) { // value
      if (isDelta) value = Math.clamped(current.min, Number(current.value) + value, current.max);
    } else { // min or max
      if (isDelta) value = Number(current) + value;
    }

    // redirect updates to setConditions
    return await this.setConditions(value, `data.${attribute}`);
    // required to update the token
    await this.update({ [`data.${attribute}${isBar ? '.value' : ''}`]: value });
    if (this.token) {
      if (this.token.bars) {
        this.token.drawBars();
      } else {
        console.error("This token had no bars: ", this.token);
      }
    }
  }

  /**
   * Fetches the total modifier value for a rollable item
   */
  fetchRelevantModifiers(name, type) {
    let tempMods = 0;
    const actormods = this.data.items.filter(i => (i.type == "Modifier"));

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.data;

      if (moddata.data.inEffect) {

        for (let entry of Object.values(moddata.data.entries)) {

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          // making it possible to modify specific attacks, defences, etc.
          switch (type) {
            case "check":
              if (entry.category == "check" && hasRelevantTarget) tempMods += entry.value;
              break;
            case "skill":
            case "technique":
              if (entry.category == "skill" && hasRelevantTarget) tempMods += entry.value;
              break;
            case "spell":
            case "rms":
              if (entry.category == "spell" && hasRelevantTarget) tempMods += entry.value;
              break;
            case "attack":
              if (entry.category == "attack" && hasRelevantTarget) tempMods += entry.value;
              break;
            case "dodge":
            case "block":
            case "parry":
            case "defence":
              if (entry.category == "defence" && hasRelevantTarget) tempMods += entry.value;
              break;
            case "reaction":
              if (entry.category == "reaction" && hasRelevantTarget) tempMods += entry.value;
              break;
            case "damage":
              if (entry.category == "damage" && hasRelevantTarget) tempMods += entry.value;
              break;
          }
        }
      }
    }
    return tempMods;
  }
}

/**
 * Counting the ranks listed in Variable items whose names start with "Ranks: "
 */
export const countRanks = function (actor) {
  if (CONFIG.grpga.testMode) console.debug("Counting Ranks");
  let actoritems = actor.data.items;
  let actorranks = {};
  let rankitems = [];

  for (const item of actoritems) {
    const itemdata = item.data
    switch (itemdata.type) {
      // add any items whose name starts with "Ranks:" to the list to be polled next
      case "Variable": {
        if (itemdata.name.startsWith("Ranks:")) rankitems.push(itemdata);
        break;
      }
      // poll all items and set an actorranks value of 0 for each Rollable and Defence
      case "Defence":
      case "Rollable": {
        let slug = actor.slugify(itemdata.name);
        actorranks[slug] = 0;
        break;
      }
    }
  }

  if (CONFIG.grpga.testMode) console.debug("Rankitems to be polled:\n", [actor, rankitems]);

  // poll the rank items
  for (const itemdata of rankitems) {
    for (let entry of Object.values(itemdata.data.entries)) {
      let target = actor.slugify(entry.label);
      // if the name is not blank
      if (target) {
        entry.value = Number(entry.formula);
        actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
      }
    }
  }

  if (CONFIG.grpga.testMode) console.debug("Rankdata after polling:\n", [actor, actorranks]);

  for (const [name, ranks] of Object.entries(actorranks)) {
    if (actor.data.data.dynamic[name]) {
      let item = actor.items.get(actor.data.data.dynamic[name]._id);
      let itemdata = item.data.data;
      // the ranks and formula are both zero. Nothing to do here.
      if (ranks == 0 && itemdata.formula == "#0" && itemdata.moddedvalue == 0) continue;
      // the ranks and formula are already the same. Nothing to do here.
      if (`#${ranks}` == itemdata.formula) continue;

      // calculate rank bonus to assign to value and moddedvalue
      let value = 0;
      let moddedvalue = 0;
      if (ranks > 20) {
        value = 70 + ranks - 20;
      } else if (ranks > 10) {
        value = 50 + (ranks - 10) * 2;
      } else {
        value = ranks * 5;
      }
      let data = {
        "data.formula": `#${ranks}`,
        "data.value": value,
        "data.moddedvalue": moddedvalue
      }
      item.update(data);
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were assigned to : ${name}, which had ${itemdata.formula} ranks`);
      // this seems to cause a bit of recursion but should be more efficient than the item.update which calls it anyway.
      //actor.updateEmbeddedDocuments("Item", { id: actordata.data.dynamic[name].id, 'data.formula': (ranks == 0) ? 0 : `#${ranks}` });
    } else {
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
    }
  }
}

/**
 * Erasing the # symbol from all items in an actor"
 */
export const resetRanks = function (actor) {
  if (CONFIG.grpga.testMode) console.debug("Erasing Ranks");
  let actoritems = actor.data.items;
  let updates = [];

  for (const item of actoritems) {
    const itemdata = item.data
    try { // if the item has a formula and that formula includes #
      if (itemdata.data.formula?.includes("#")) {
        let data = {
          "data.formula": "0",
          "data.value": 0,
          "data.moddedvalue": 0
        }
        item.update(data);
      }
    } catch (err) { // the formula is an integer, replace it
      let data = {
        "data.formula": "0",
        "data.value": 0,
        "data.moddedvalue": 0
      }
      item.update(data);
    }
  }
  return;
  for (const [name, ranks] of Object.entries(actorranks)) {
    if (actor.data.data.dynamic[name]) {
      let item = actor.items.get(actor.data.data.dynamic[name]._id);
      let itemdata = item.data.data;
      // the ranks and formula are both zero. Nothing to do here.
      if (ranks == 0 && itemdata.formula == "#0" && itemdata.moddedvalue == 0) continue;
      // the ranks and formula are already the same. Nothing to do here.
      if (`#${ranks}` == itemdata.formula) continue;

      // calculate rank bonus to assign to value and moddedvalue
      let value = 0;
      let moddedvalue = 0;
      if (ranks > 20) {
        value = 70 + ranks - 20;
      } else if (ranks > 10) {
        value = 50 + (ranks - 10) * 2;
      } else {
        value = ranks * 5;
      }
      let data = {
        "data.formula": `#${ranks}`,
        "data.value": value,
        "data.moddedvalue": moddedvalue
      }
      item.update(data);
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were assigned to : ${name}, which had ${itemdata.formula} ranks`);
      // this seems to cause a bit of recursion but should be more efficient than the item.update which calls it anyway.
      //actor.updateEmbeddedDocuments("Item", { id: actordata.data.dynamic[name].id, 'data.formula': (ranks == 0) ? 0 : `#${ranks}` });
    } else {
      if (CONFIG.grpga.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
    }
  }
}
