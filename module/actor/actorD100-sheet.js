import { grpga } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorD100Sheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorD100-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.grpga.testMode) console.debug("entering getData() in ActorD100-sheet");

    let data = super.getData(options);
    let actor = data.actor;
    let actordata = actor.data;

    data.headerinfo = {};
    let headinfo = data.headerinfo;

    headinfo.parry = actordata.tracked.parry;
    headinfo.hitpoints = actordata.tracked.hp;
    headinfo.woundsbleed = actordata.dynamic.woundsbleed.value;
    headinfo.woundspen = actordata.dynamic.woundspen.value;
    headinfo.armourtype = actordata.defence.armourType;
    headinfo.melee = actordata.defence.meleeDB;
    headinfo.missile = actordata.defence.missileDB;
    headinfo.declaration = this.actor.items.get(actordata.dynamic["declared-action"]?._id)?.data;
    headinfo.move = this.actor.items.get(actordata.dynamic["move-rate"]?._id)?.data;
    // move-rate formulae aren't processed in time to display the value. Get it from dynamic.
    if (headinfo.move) {
      headinfo.move.rate = actordata.dynamic["move-rate"]?.value;
      headinfo.move.mode = actordata.dynamic["move-mode"]?.label[0];
    }
    headinfo.level = actordata.dynamic["level-ranks"]?.value;

    for (let item of Object.values(actordata.dynamic)) {
      if (item.name?.startsWith("Culture")) {
        headinfo.culture = item.name.split(": ")[1].trim() || "";
      } else if (item.name?.startsWith("Profession: ")) {
        headinfo.vocation = item.name.split(": ")[1].trim() || "";
      } else if (item.name?.startsWith("Race: ")) {
        headinfo.kin = item.name.split(": ")[1].trim() || "";
      }
    }
    for (let item of data.reactionmods) {
      if (item.name?.includes("Shield")) {
        headinfo.meshield = headinfo.mishield = 0;
        if (!item.data.inEffect) continue;
        for (let entry of Object.values(item.data.entries)) {
          if (entry.targets == "Melee") headinfo.meshield = entry.value;
          else if (entry.targets == "Missile") headinfo.mishield = entry.value;
        }
      }
    }

    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importRM').click(this._onImportRMData.bind(this));
  }

  async _onImportRMData(event) {
    event.preventDefault();
    const scriptdata = this.actor.data.data.itemscript.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.data.data.biography;

    for (let entry of scriptdata) {
      const line = entry.split(":");
      switch (line[0].trim()) {
        case "Name": {
          await this.actor.update({ 'name': line[1].trim() });
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.data.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "MeleeDB": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.data.items.find(i => i.name === "Melee DB");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "MissileDB": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.data.items.find(i => i.name === "Missile DB");
          const update = { _id: item.id, 'data.formula': num, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[1].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Culture": { // Rollable: check
          let str = line[1].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Profession": { // Rollable: check
          let str = line[1].trim();
          const item = this.actor.data.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'data.formula': 0, 'data.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Various": {
          let data = {
            type: "Defence",
            data: {
              chartype: "CharacterD100",
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1];
          switch (result[2]?.toLowerCase()) {
            case "oe": data.data.category = "block"; break;
            case "oeh": data.data.category = "parry"; break;
            default: data.data.category = "dodge"; break;
          }
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Skill":
        case "Spell":
        case "Check": {
          let data = {
            type: "Rollable",
            data: {
              chartype: "CharacterD100",
              category: line[0].toLowerCase(),
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1];
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Primary": {
          let data = {
            type: "Primary-Attribute",
            data: {
              chartype: "CharacterD100",
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1] || this.actor.slugify(result[0]);
          data.data.attr = Number(result[2]) || 0;
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Melee": {
          let data = {
            type: "Melee-Attack",
            data: {
              chartype: "CharacterD100"
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || 0;
          data.data.armourDiv = Number(result[2]) || 0;
          data.data.weight = Number(result[3]) || 0;
          data.data.damage = result[4];
          data.data.damageType = result[5];
          data.data.minST = result[6] || "oeh";
          data.data.reach = Number(result[7]) || 5;
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Ranged": {
          let data = {
            type: "Ranged-Attack",
            data: {
              chartype: "CharacterD100"
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1] || 0;
          data.data.armourDiv = Number(result[2]) || 0;
          data.data.accuracy = Number(result[3]) || 0;
          data.data.range = result[4] || "5";
          data.data.damage = result[5];
          data.data.damageType = result[6];
          data.data.rof = Number(result[7]) || 5;
          data.data.minST = result[8] || "oeh";
          data.data.notes = entry.trim();
          const item = this.actor.data.items.find(i => i.name === result[0]);
          if (item) {
            data._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [data]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'data.biography': biography });
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  async _onRoll(event) {
    event.preventDefault();
    game.settings.set("grpga", "currentActor", this.actor.id);
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.d100.${dataset.type}`, { name: dataset.name });
    let unmodified = dataset.threat || 0;
    let openEndedRange = Number(dataset.oerange) || 5;
    let rollValue = Number(dataset.roll);
    let rollType = dataset.rolltype || "std";
    let expRoll = 0; // for possible exploding dice
    let critRoll = 0; // for possible critical roll
    let isReaction = false;
    let isSpecific = false;

    let formula = "";
    switch (dataset.type) {
      case "dodge": { // Standard D100 from Various Rolls
        rollType = "std";
        formula = "1d100 + " + rollValue;
        flavour += ` [<b>${rollValue}</b>]`;
        isSpecific = true;
        break;
      }
      case "block": { // Open-Ended D100 from Various Rolls
        rollType = "oe";
        formula = "1d100 + " + rollValue;
        flavour += ` [<b>${rollValue}</b>]`;
        isSpecific = true;
        break;
      }
      case "parry": { // Open-Ended High D100 from Various Rolls
        rollType = "oeh";
        formula = "1d100 + " + rollValue;
        flavour += ` [<b>${rollValue}</b>]`;
        isSpecific = true;
        break;
      }
      case "reaction": { // For Melee or Missile Defensive Bonus "rolls"
        formula = rollValue;
        flavour += ` [<b>${rollValue}</b>]`;
        isReaction = true;
        break;
      }
      case "attack": { // Open-Ended
        formula = "1d100 + " + rollValue;
        flavour += ` [<b>${rollValue}</b>]`;
        try {
          Journal._showEntry(game.journal.getName(dataset.attacktable).uuid, "text", true);
        } catch (err) {
          if (dataset.attacktable) ui.notifications.error(`There is no matching Journal Entry for ${dataset.attacktable}`);
        }
        break;
      }
      default: { // the D100 roll and skill value/level/bonus
        formula = "1d100 + " + rollValue;
        flavour += ` [<b>${rollValue}</b>]`;
        break;
      }
    }

    // get the modifiers
    let modList = this.fetchRelevantModifiers(this.actor.data, dataset);
    // process the modifiers
    let modformula = "";
    let hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += `<p class="chatmod">`;
      for (const mod of modList) {
        sign = (mod.modifier > -1) ? "+" : "";
        modformula += sign + mod.modifier;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    // process the original roll
    let roll = await new Roll(formula + modformula).evaluate({ async: true });
    let firstRoll = roll.terms[0].total;
    // prepare the crit roll
    critRoll = await new Roll("1d100").evaluate({ async: true });

    if (!isReaction) {
      if (firstRoll <= unmodified) {
        // fumble has occurred
        // render roll and critRoll with fumble message then return
        flavour += `<p class="critfail">D100 Roll: [<b>${firstRoll} - Critical Failure</b>]</p>`;
        flavour += `<p>D100 Crit Roll: [<b>${critRoll.total}</b>]</p>`;
        roll.toMessage(
          {
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            flavor: flavour
          });
        this.actor.update({ ["data.gmod.value"]: 0 });
        return;
      }

      expRoll = await new Roll("1d100x>95").evaluate({ async: true });

      // display the d100 roll
      flavour += `<p>D100 Roll: [<b>${firstRoll}</b>]`;

      // write the link to the attack table if there is one
      try {
        game.journal.getName(dataset.attacktable).uuid;
        flavour += ` - <a class="open-journal" data-table="${dataset.attacktable}">Table</a></p>`;
      } catch (err) {
        // if it is broken, this will appear twice for attacks but only once for other rolls
        if (dataset.attacktable) ui.notifications.error(`There is no matching Journal Entry for ${dataset.attacktable}`);
      }
      // assemble the new formula less possible exploding result
      let newform = `${firstRoll}+${rollValue}${modformula}`;
      if (firstRoll <= openEndedRange) {
        // does this explode down?
        switch (rollType) {
          case "oe":
          case "oel": {
            newform += `-${expRoll.total}`;
            flavour += `<p class="critfail">${rollType.toUpperCase()} Roll: [<b>${expRoll.total}</b>]</p>`;
            break;
          }
        }
      } else if (firstRoll > 100 - openEndedRange) {
        // does this explode up?
        switch (rollType) {
          case "oe":
          case "oeh": {
            newform += `+${expRoll.total}`;
            flavour += `<p class="critsuccess">${rollType.toUpperCase()} Roll: [<b>${expRoll.total}</b>]</p>`;
            break;
          }
        }
      }
      roll = await new Roll(newform).evaluate({ async: true });

      // display the automated crit fail roll for attacks and default rolls if desired
      if (!isSpecific && this.actor.data.data.autoCrit) {
        flavour += `<p>D100 Crit Roll: [<b>${critRoll.total}</b>]`;
        try {
          game.journal.getName(dataset.crittable).uuid;
          flavour += ` - <a class="open-journal" data-table="${dataset.crittable}">Table</a></p>`;
        } catch (err) {
          if (dataset.crittable) ui.notifications.error(`There is no matching Journal Entry for ${dataset.crittable}`);
        }
      }
      if (roll.total > dataset.maxresult) {
        flavour += `<p class="critfail">${game.i18n.localize(`grpga.rolls.attack.maxresult`)}: [<b>${dataset.maxresult}</b>]</p>`;
      }
    } else {
      // this is a defence bonus roll so update the stored defence values
      let defence = this.actor.data.defence || {};
      defence.armourType = this.actor.data.items.find(i => i.name == "Armor Type").data.formula;
      if (dataset.name == "Missile DB") {
        defence.missileDB = roll.total;
      } else {
        defence.meleeDB = roll.total;
      }
      this.actor.update({ ["data.defence"]: defence });
    }

    // prepare the flavor in advance based on which type of die is in use.
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour
      }
    );
    this.actor.update({ ["data.gmod.value"]: 0 });
    return;
  }
}
