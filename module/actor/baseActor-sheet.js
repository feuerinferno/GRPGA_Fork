/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class baseActorSheet extends ActorSheet {

  itemContextMenu = [
    {
      name: game.i18n.localize("grpga.sheet.edit"),
      icon: '<i class="fas fa-edit"></i>',
      callback: element => {
        const item = this.actor.items.get(element.data("item-id"));
        item.sheet.render(true);
      }
    },
    {
      name: game.i18n.localize("grpga.sheet.delete"),
      icon: '<i class="fas fa-trash"></i>',
      callback: element => {
        this.actor.deleteEmbeddedDocuments("Item", [element.data("item-id")]);
      }
    }
  ];

  /** @override */
  getData() {
    if (CONFIG.grpga.testMode) console.debug("entering getData() in baseActor-sheet\n", this);

    const data = super.getData();
    const actorData = this.actor.data.toObject(false);
    data.actor = actorData;
    data.data = actorData.data;
    data.rollData = this.actor.getRollData.bind(this.actor);
    data.items = actorData.items;

    if (data.data.userGroupSort) {
      data.items.sort((a, b) => {
        if (a.data.sort < b.data.sort) return -1;
        if (a.data.sort > b.data.sort) return 1;
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });
    } else {
      data.items.sort((a, b) => {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });
    }
    data.config = CONFIG.grpga;

    data.mode = actorData.data.mode || data.config.ruleset;
    data.modeSpecificText = {
      mode: data.mode,
      stepT: game.i18n.localize(`grpga.actor.bm.${data.mode}.stepT`),
      step: game.i18n.localize(`grpga.actor.bm.${data.mode}.step`),
      quarterT: game.i18n.localize(`grpga.actor.bm.${data.mode}.quarterT`),
      quarter: game.i18n.localize(`grpga.actor.bm.${data.mode}.quarter`),
      halfT: game.i18n.localize(`grpga.actor.bm.${data.mode}.halfT`),
      half: game.i18n.localize(`grpga.actor.bm.${data.mode}.half`),
      moveT: game.i18n.localize(`grpga.actor.bm.${data.mode}.moveT`),
      move: game.i18n.localize(`grpga.actor.bm.${data.mode}.move`),
      sprintT: game.i18n.localize(`grpga.actor.bm.${data.mode}.sprintT`),
      sprint: game.i18n.localize(`grpga.actor.bm.${data.mode}.sprint`),
      advantages: game.i18n.localize(`grpga.sections.${data.mode}.advantages`),
      disadvantages: game.i18n.localize(`grpga.sections.${data.mode}.disadvantages`),
      perks: game.i18n.localize(`grpga.sections.${data.mode}.perks`),
      quirks: game.i18n.localize(`grpga.sections.${data.mode}.quirks`),
      defences: game.i18n.localize(`grpga.sections.${data.mode}.defences`),
      checks: game.i18n.localize(`grpga.sections.${data.mode}.checks`),
      hitlocations: game.i18n.localize(`grpga.sections.${data.mode}.hitlocations`),
      bsabbr: game.i18n.localize(`grpga.actor.bs.${data.mode}.abbr`),
      bsname: game.i18n.localize(`grpga.actor.bs.${data.mode}.name`),
      armourDiv: game.i18n.localize(`grpga.actor.melee-attack.${data.mode}.armourDiv`),
      minST: game.i18n.localize(`grpga.actor.melee-attack.${data.mode}.minST`),
      reach: game.i18n.localize(`grpga.actor.melee-attack.${data.mode}.reach`),
      range: game.i18n.localize(`grpga.actor.ranged-attack.${data.mode}.range`),
    }

    data.primaryattributes = [];
    data.equipment = [];
    data.hitlocations = [];
    data.skills = [];
    data.spells = [];
    data.checks = [];
    data.advantages = [];
    data.disadvantages = [];
    data.perks = [];
    data.quirks = [];
    data.attacks = [];
    data.defences = [];
    data.pools = [];
    data.containers = [];
    data.modifiers = [];
    data.variables = [];
    data.customtabs = [];
    data.attackdamagemods = [];
    data.defencemods = [];
    data.reactionmods = [];
    data.skillmods = [];
    data.spellmods = [];
    data.checkmods = [];
    data.primods = [];

    for (let item of data.items) {
      switch (item.type) {
        case "Primary-Attribute": {
          data.primaryattributes.push(item);
          break;
        }
        case "Equipment": {
          data.equipment.push(item);
          break;
        }
        case "Hit-Location": {
          data.hitlocations.push(item);
          break;
        }
        case "Rollable": {
          switch (item.data.category) {
            case "skill":
            case "technique": {
              data.skills.push(item);
              break;
            }
            case "spell":
            case "rms": {
              data.spells.push(item);
              break;
            }
            case "check": {
              data.checks.push(item);
              break;
            }
          }
          break;
        }
        case "Trait": {
          switch (item.data.category) {
            case "advantage": {
              data.advantages.push(item);
              break;
            }
            case "disadvantage": {
              data.disadvantages.push(item);
              break;
            }
            case "quirk": {
              data.quirks.push(item);
              break;
            }
            case "perk": {
              data.perks.push(item);
              break;
            }
          }
          break;
        }
        case "Ranged-Attack":
        case "Melee-Attack": {
          data.attacks.push(item);
          break;
        }
        case "Defence": {
          data.defences.push(item);
          break;
        }
        case "Pool": {
          data.pools.push(item);
          break;
        }
        case "Container": {
          data.containers.push(item);
          break;
        }
        case "Variable": {
          data.variables.push(item);
          break;
        }
        case "Custom-Tab": {
          data.customtabs.push(item);
          break;
        }
        case "Modifier": {
          data.modifiers.push(item);
          if (item.data.attack || item.data.damage) data.attackdamagemods.push(item);
          if (item.data.defence) data.defencemods.push(item);
          if (item.data.reaction) data.reactionmods.push(item);
          if (item.data.skill) data.skillmods.push(item);
          if (item.data.spell) data.spellmods.push(item);
          if (item.data.check) data.checkmods.push(item);
          if (item.data.primary) data.primods.push(item);
          break;
        }
      }
    }

    if (data.mode == "d20") {
      // spell list filter for d20
      var spellLevel = data.checks.filter(i => (i.name == "Level" && i.data.formula != ""))[0];
      if (spellLevel) {
        data.spells = data.spells.filter(i => this.checkSpellTypeLevel(i, spellLevel.data.formula));
      }
    }

    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.item-export').click(this._onItemExport.bind(this));
    html.find('.iteminput').change(this._onInputChange.bind(this));
    html.find('.plus').click(this._onPlusMinus.bind(this));
    html.find('.minus').click(this._onPlusMinus.bind(this));
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.item-edit').click(this._onItemEdit.bind(this));
    html.find('.item-delete').click(this._onItemDelete.bind(this));
    html.find('.item-toggle').click(this._onToggleItem.bind(this));
    html.find('.collapsible').click(this._onToggleCollapse.bind(this));

    new ContextMenu(html, ".item", this.itemContextMenu);

    let handler = ev => this._onDragStart(ev);
    html.find('.draggable').each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    html.on('click', '.open-journal', ev => {
      ev.preventDefault(); // TODO: does this need to be here?
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch (err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });

  }

  _onToggleCollapse(event) {
    event.preventDefault();
    if (CONFIG.grpga.testMode) console.debug("entering _onToggleCollapse()", [this, event]);
    // do not collapse when creating an item
    if (event.target.parentElement.className.includes('item-create')) return;
    if (event.target.parentElement.className.includes('char-name')) return;
    let section = event.currentTarget.dataset.section;
    let current = this.actor.data.data.sections[section];
    let update = `data.sections.${section}`;
    let data = (current === "block") ? "none" : "block";
    this.actor.update({ [update]: data });
  }

  async _onPlusMinus(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onPlusMinus()", [this, event]);

    let field = event.currentTarget.firstElementChild;
    let actorData = this.actor.data.data;
    let fieldName = field.name;
    let change = Number(field.value);
    var value;
    var fieldValue;

    if (field.className.includes("pool")) {
      return this.actor.modifyTokenAttribute(`tracked.${fieldName.toLowerCase()}`, Number(field.value), true, true);

    } else { // haven't figured out non-pools yet

      switch (fieldName) {
        case "gmod": {
          fieldValue = "data.gmod.value";
          value = change + actorData.gmod.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        case "IS": {
          fieldValue = "data.is.value";
          value = change + actorData.is.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        case "DLtH": {
          fieldValue = "data.dlth.value";
          value = change + actorData.dlth.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        default: {
          break;
        }
      }
    }

  }

  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    let attr = "";
    switch (item.type) {
      case "Modifier": {
        attr = "data.inEffect";
        break;
      }
      default: {
        ui.notifications.warning(`Toggling of ${item.type} is not yet supported.`)
      }
    }
    return item.update({ [attr]: !getProperty(item.data, attr) });
  }

  _onItemEdit(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".item").dataset.itemId;
    let item = this.actor.items.get(itemId);
    item.sheet.render(true);
  }

  _onItemDelete(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".item").dataset.itemId;
    this.actor.deleteEmbeddedDocuments("Item", [itemId]);
  }

  /** @override */
  _onDragStart(event) {
    // changed it to target the parentElement of the event target
    const li = event.currentTarget.parentElement;

    // the rest is the same in Foundry 0.8.6
    if (event.target.classList.contains("entity-link")) return;

    // Create drag data
    const dragData = {
      actorId: this.actor.id,
      sceneId: this.actor.isToken ? canvas.scene?.id : null,
      tokenId: this.actor.isToken ? this.actor.token.id : null
    };

    // Owned Items
    if (li.dataset.itemId) {
      const item = this.actor.items.get(li.dataset.itemId);
      dragData.type = "Item";
      dragData.data = item.data;
    }

    // Active Effect
    if (li.dataset.effectId) {
      const effect = this.actor.effects.get(li.dataset.effectId);
      dragData.type = "ActiveEffect";
      dragData.data = effect.data;
    }

    // Set data transfer
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  async getNamedItem(name) {
    let data = this.actor.data.data;
    let parts = name.split(".");
    if (parts.length != 2) return undefined;
    return await this.actor.items.get(data[parts[0]][parts[1]]?._id);
  }

  async dropItem(item) {
    let baseitem = await this.actor.items.get(item[0].id);
    if (baseitem.data.type == "Container") {
      await this._createItems(baseitem.data.data.notes);
      this.actor.deleteEmbeddedDocuments("Item", [item[0].id]);
    } else {
      baseitem.sheet.render(true);
    }
    return baseitem;
  }

  /** @override */
  async _onDrop(event) {
    // wait for the item to be copied to the actor
    let item = await super._onDrop(event);
    if (item) { // a proper item was dropped and identified
      return this.dropItem(item);
    } else { // a piece of non-item data was dropped
      if (CONFIG.grpga.testMode) console.debug("Data drop on Actor not handled by baseActorSheet");
    }
  }

  async _onTokenDrop(dragItem) {
    // wait for the item to be copied to the actor
    if (dragItem.type == "Item") {
      let item = await this._onDropItem(null, dragItem);
      return this.dropItem(item);
    } else {
      if (CONFIG.grpga.testMode) console.debug("Data drop on Token not handled by baseActorSheet");
    }
  }

  _onInputChange(event) {
    event.preventDefault();

    if (CONFIG.grpga.testMode) console.debug("entering _onInputChange()", [this, event]);

    const target = event.currentTarget;
    // get the item id
    const itemId = target.parentElement.attributes["data-item-id"]?.value;
    // get the name of the changed element
    let dataname = target.attributes["data-name"].value;
    // get the new value
    let value = (target.type === "checkbox") ? target.checked : target.value;
    // Get the original item.
    const item = this.actor.items.get(itemId);

    if (dataname == "data.alwaysOn") {
      item.update({ "data.alwaysOn": value, "data.inEffect": value });
      return;
    }

    // redirect changes to pool values
    if (target.className.includes("pool")) {
      let isDelta = (value.startsWith("+") || value.startsWith("-"));
      return this.actor.modifyTokenAttribute(
        `tracked.${target.dataset.abbr.toLowerCase()}`,
        Number(value),
        isDelta, true
      );
    }

    // TODO: this may not be necessary in 0.8.6
    // Variables owned by unlinked actors do not use preUpdateOwnedItem so we need to do it here
    if (item?.type == "Variable" && !this.token?.data.actorLink) {
      if (dataname == 'data.label') {
        dataname = 'data';
        let updata = item.data.data;
        updata.label = value;
        // we have selected a new value
        let entries = Object.values(updata.entries);
        for (let i = 1; i < entries.length; i++) { //ignore category changes to [0]
          if (entries[i].label == updata.label) {
            updata.value = entries[i].value;
            updata.formula = entries[i].formula;
            break;
          }
        }
        value = updata;
      }
    }

    // update the item with the new value for the element or the actor if it is gmod
    (item) ? item.update({ [dataname]: value }) : this.actor.update({ [dataname]: value });
  }

  checkSpellTypeLevel(spell, level) {
    var levelString = spell.data.notes.match("<p><b>Level</b>:[^<]*");
    return levelString[0].match(level);
  }

  _onItemCreate(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const scriptdata = this.actor.data.data.itemscript || "";
    if (!dataset.type) {
      this._createItems(scriptdata);
    } else {
      this._createItem(dataset);
    }
  }

  _onItemExport() {
    let items = duplicate(this.actor.data.items);
    var exported = [];
    for (var itemData of items) {
      if (itemData.type == "Container") continue;
      delete itemData._id;
      delete itemData.flags;
      delete itemData.sort;
      delete itemData.effects;
      exported.push(itemData);
    }
    exported = JSON.stringify(exported);
    this.actor.update({ "data.itemscript": exported });
  }

  async _createItems(scriptdata) {
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata.replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    await this.actor.createEmbeddedDocuments('Item', JSON.parse(scriptdata));
    ui.notifications.info("Creation of multiple items completed!");
  }

  async _createItem(dataset) {
    let data = {
      name: dataset.type,
      type: dataset.type,
      data: {
        chartype: this.actor.data.type,
      }
    };
    if (dataset.category) data.data.category = dataset.category;
    if (dataset.type == "Modifier") {
      data.data.attack = (dataset.mod1 == "attack" || dataset.mod2 == "attack");
      data.data.defence = (dataset.mod1 == "defence" || dataset.mod2 == "defence");
      data.data.skill = (dataset.mod1 == "skill" || dataset.mod2 == "skill");
      data.data.spell = (dataset.mod1 == "spell" || dataset.mod2 == "spell");
      data.data.check = (dataset.mod1 == "check" || dataset.mod2 == "check");
      data.data.reaction = (dataset.mod1 == "reaction" || dataset.mod2 == "reaction");
      data.data.damage = (dataset.mod1 == "damage" || dataset.mod2 == "damage");
      data.data.primary = (dataset.mod1 == "primary" || dataset.mod2 == "primary");
    }
    return await this.actor.createEmbeddedDocuments('Item', [data], { renderSheet: true });
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRelevantModifiers(actorData, dataset) {
    let tempMods = [];
    const actormods = actorData.items.filter(i => (i.type == "Modifier"));

    // preload gmod
    tempMods.push({ modifier: actorData.data.gmod.value || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.data;

      if (moddata.data.inEffect) {

        for (let entry of Object.values(moddata.data.entries)) {

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          let type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

          // making it possible to modify specific attacks, defences, etc.
          switch (type) {
            case "check":
              if (entry.category == "check" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
            case "skill":
            case "technique":
              if (entry.category == "skill" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
            case "spell":
            case "rms":
              if (entry.category == "spell" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
            case "attack":
              if (entry.category == "attack" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
            case "dodge":
            case "block":
            case "parry":
            case "defence":
              if (entry.category == "defence" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
            case "reaction":
              if (entry.category == "reaction" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
            case "damage":
              if (entry.category == "damage" && hasRelevantTarget) tempMods.push({ modifier: entry.value, description: moddata.data.tempName });
              break;
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: Number(mod.modifier, 10) })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }
}