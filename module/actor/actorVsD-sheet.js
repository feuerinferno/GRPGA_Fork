import { grpga } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorVsDSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorVsD-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.grpga.testMode) console.debug("entering getData() in ActorVsD-sheet");

    let data = super.getData(options);
    let actor = data.actor;
    let actordata = actor.data;

    // temporary implementation of a radio button choices
    data.test = {
      groupName: "data.bm.step",
      choices: { 1: "ChoiceA", 2: "ChoiceB" },
      chosen: actordata.bm.step || 1
    }

    data.headerinfo = {};
    let headinfo = data.headerinfo;

    headinfo.parry = actordata.tracked.parry;
    headinfo.hitpoints = actordata.tracked.hp;
    headinfo.woundsbleed = actordata.dynamic.woundsbleed.value;
    headinfo.woundspen = actordata.dynamic.woundspen.value;
    headinfo.armourtype = actordata.defence.armourType;
    headinfo.melee = actordata.defence.meleeDB;
    headinfo.missile = actordata.defence.missileDB;
    headinfo.declaration = this.actor.items.get(actordata.dynamic["declared-action"]?._id)?.data;
    headinfo.move = this.actor.items.get(actordata.dynamic["move-rate"]?._id)?.data;
    // move-rate formulae aren't processed in time to display the value. Get it from dynamic.
    if (headinfo.move) {
      headinfo.move.rate = actordata.dynamic["move-rate"]?.value;
      headinfo.move.mode = actordata.dynamic["move-mode"]?.label[0];
    }
    headinfo.level = actordata.dynamic["level-ranks"]?.value;

    for (let item of data.disadvantages) {
      if (item.name?.startsWith("All")) {
        headinfo.allegiance = item.data.notes;
      } else if (item.name?.startsWith("Mot")) {
        headinfo.motivation = item.data.notes;
      } else if (item.name?.startsWith("Nat")) {
        headinfo.nature = item.data.notes;
      }
    }
    for (let item of Object.values(actordata.dynamic)) {
      if (item.name?.startsWith("Ranks: Culture")) {
        let regex = new RegExp(/\(([\w]+)\)/);
        let result = regex.exec(item.name);
        headinfo.culture = (result) ? result[1] : "";
      } else if (item.name?.startsWith("Vocation: ")) {
        headinfo.vocation = item.name.split(": ")[1].trim() || "";
      }
    }
    for (let item of data.primods) {
      if (item.name?.startsWith("Kin: ")) {
        headinfo.kin = item.name.split(": ")[1].trim() || "";
      }
    }
    for (let item of data.reactionmods) {
      if (item.name?.includes("Shield")) {
        headinfo.meshield = headinfo.mishield = 0;
        if (!item.data.inEffect) continue;
        for (let entry of Object.values(item.data.entries)) {
          if (entry.targets == "Melee") headinfo.meshield = entry.value;
          else if (entry.targets == "Missile") headinfo.mishield = entry.value;
        }
      }
    }

    // get the Primary Attributes for display
    data.primarysort = {
      name: grpga.vsd.primarysort.name
    };
    let pridata = [];
    for (let skillname of grpga.vsd.primarysort.data) {
      let skill = actordata.dynamic[skillname];
      if (skill) {
        pridata.push(skill);
      }
    }
    data.primarysort.data = pridata;

    // get the advancement pools
    // fetch the named variable in dynamic and use the data stored there
    data.advancementpools = [];
    for (let varname of grpga.vsd.advancementpools) {
      if (actordata.dynamic[varname]) data.advancementpools.push(actordata.dynamic[varname]);
    }
    // find the named variable id in dynamic then use the original item
    data.advancementvariables = [];
    for (let variable of data.variables) {
      if (variable.name.startsWith("Ranks:") || variable.name.startsWith("Vocation:")) data.advancementvariables.push(variable);
    }

    // mark all of these as standard skills
    data.skillsort = [];
    for (let skillcategory of grpga.vsd.skillsort) {
      let category = {
        name: skillcategory.name
      };
      let skilldata = [];
      for (let skillname of skillcategory.data) {
        let skill = actordata.dynamic[skillname];
        if (skill) {
          skill.standard = true;
          skilldata.push(skill);
        }
      }
      category.data = skilldata;
      data.skillsort.push(category);
    }
    // fetch the named variable in dynamic and use the data stored there; mark Body and Armor as standard
    data.oddskills = [];
    for (let varname of grpga.vsd.oddskills) {
      if (actordata.dynamic[varname]) {
        data.oddskills.push(actordata.dynamic[varname]);
        actordata.dynamic[varname].standard = true;
      };
    }
    data.skillspecials = [];
    data.specialrolls = [];
    // fetch the named variable in dynamic and use the data stored there
    for (let varname of grpga.vsd.specialrolls) {
      if (actordata.dynamic[varname]) data.specialrolls.push(actordata.dynamic[varname]);
    }
    // search dynamic for the rest of the skills (and critical rolls) and use the data stored there
    for (let item of Object.values(actordata.dynamic)) {
      if (!item.standard && item.category == "skill") data.skillspecials.push(item);
      else if (item.type == "Defence" && item.name.startsWith("Critical")) data.specialrolls.push(item);
    }

    // find the named variable id in dynamic then use the original item
    data.skillvariables = [];
    for (let varname of grpga.vsd.skillvariables) {
      if (actordata.dynamic[varname]) data.skillvariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }
    // search the attack items array and filter for attack spells (clumsy range is 10)
    data.attackspells = [];
    for (let item of data.attacks) {
      if (item.data.armourDiv == 10) {
        item.spellnamecrit = item.name.split(":")[1].trim();
        item.spellname = item.spellnamecrit.split("(")[0].trim();
        data.attackspells.push(item);
      }
    }
    // find the named variable id in dynamic then use the original item
    data.spellvariables = [];
    for (let varname of grpga.vsd.spellvariables) {
      if (actordata.dynamic[varname]) data.spellvariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }
    // fetch the named variable in dynamic and use the data stored there
    data.spellpools = [];
    for (let varname of grpga.vsd.spellpools) {
      if (actordata.dynamic[varname]) data.spellpools.push(actordata.dynamic[varname]);
    }
    // find the named variable id in dynamic then use the original item
    data.specialmods = [];
    for (let varname of grpga.vsd.specialmods) {
      if (actordata.dynamic[varname]) data.specialmods.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }

    // find the named variable id in dynamic then use the original item
    data.defencevariables = [];
    for (let varname of grpga.vsd.defencevariables) {
      if (actordata.dynamic[varname]) data.defencevariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }
    // fetch the named variable in dynamic and use the data stored there
    data.defencesort = [];
    for (let varname of grpga.vsd.defencesort) {
      if (actordata.dynamic[varname]) data.defencesort.push(actordata.dynamic[varname]);
    }
    // fetch the named variable in dynamic and use the data stored there
    data.savesort = [];
    for (let varname of grpga.vsd.savesort) {
      if (actordata.dynamic[varname]) data.savesort.push(actordata.dynamic[varname]);
    }
    // fetch the named variable in dynamic and use the data stored there
    data.defencepools = [];
    for (let varname of grpga.vsd.defencepools) {
      if (actordata.dynamic[varname]) data.defencepools.push(actordata.dynamic[varname]);
    }
    // find the named variable id in dynamic then use the original item
    data.defencemodifiers = [];
    for (let varname of grpga.vsd.defencemodifiers) {
      if (actordata.dynamic[varname]) data.defencemodifiers.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }
    // merge two existing arrays, removing duplicates
    data.defencesavemods = [...data.defencemods];
    for (let mod of data.reactionmods) {
      if (!data.defencesavemods.includes(mod)) data.defencesavemods.push(mod);
    }
    // search the attack items array and filter for physical attacks (clumsy range is less than 10)
    data.physicalattacks = [];
    for (let item of data.attacks) {
      if (item.data.armourDiv < 10) {
        item.attacknamecrit = item.name.split(":")[1]?.trim() || item.name;
        item.attackname = item.attacknamecrit.split("(")[0]?.trim() || item.name;
        data.physicalattacks.push(item);
      }
    }
    // fetch the named variable in dynamic and use the data stored there
    data.attackpools = [];
    for (let varname of grpga.vsd.attackpools) {
      if (actordata.dynamic[varname]) data.attackpools.push(actordata.dynamic[varname]);
    }
    // find the named variable id in dynamic then use the original item
    data.attackvariables = [];
    for (let varname of grpga.vsd.attackvariables) {
      if (actordata.dynamic[varname]) data.attackvariables.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }
    data.specialcombatrolls = [];
    // fetch the named variable in dynamic and use the data stored there
    for (let varname of grpga.vsd.specialcombatrolls) {
      if (actordata.dynamic[varname]) data.specialcombatrolls.push(actordata.dynamic[varname]);
    }
    // search dynamic for the rest of the skills (and critical rolls) and use the data stored there
    for (let item of Object.values(actordata.dynamic)) {
      if (item.type == "Defence" && item.name.startsWith("Critical")) data.specialcombatrolls.push(item);
    }
    // find the named variable id in dynamic then use the original item
    data.specialcombatmods = [];
    for (let varname of grpga.vsd.specialcombatmods) {
      if (actordata.dynamic[varname]) data.specialcombatmods.push(this.actor.items.get(actordata.dynamic[varname]._id).data);
    }


    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importVsD').click(this._onImportVsDData.bind(this));
  }

  async dropData(dragItem) {
    if (CONFIG.grpga.testMode) console.debug(`processing ${dragItem.type}\n`, dragItem);
    let data = {};
    switch (dragItem.type) {
      case "critical": {
        if (dragItem.bleed != 0 || dragItem.action != 0) { // there is a wound to process
          let severity = "na";
          let dlth = 0;
          if (dragItem.action > 50) {
            severity = "crippling";
            dlth = 33;
          } else if (dragItem.action > 20) {
            severity = "serious";
            dlth = 13;
          } else if (dragItem.action > 0) {
            severity = "minor";
            dlth = 3;
          }
          data = {
            name: dragItem.woundtitle,
            type: "Hit-Location",
            data: {
              chartype: "CharacterVsD",
              damageResistance: Number(dragItem.bleed) || 0,
              damage: -Number(dragItem.action) || 0,
              injuryType: severity,
              toHitPenalty: dlth,
              notes: dragItem.message
            }
          }
          let item = await this.actor.createEmbeddedDocuments("Item", [data]);
          const baseitem = await this.actor.items.get(item[0].id);
          baseitem.sheet.render(true);
        }
        if (dragItem.hits) { // apply damage so it will update status and bar
          await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        }
        if (dragItem.effect) { // apply an effect (TODO: make a longer list)
          if (dragItem.effect.includes("stun")) {
            let item = await this.getNamedItem("modifiers.stunned");
            await item.update({ "data.inEffect": true });
          }
        }
        break;
      }
      case "damage": { // apply damage so it will update status and bar
        await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        break;
      }
      case "severity": { // set the value of the critical severity item
        let item = await this.getNamedItem("dynamic.critical-severity");
        let parts = dragItem.crit.split(" ");
        if (parts.length == 1) {
          await item.update({ "data.label": parts[0], "data.formula": "+0", "data.value": 0 });
        } else {
          await item.update({ "data.label": parts[0], "data.formula": parts[1], "data.value": Number(parts[1]) });
        }
        break;
      }
    }
  }

  /** @override */
  async _onDrop(event) {
    // wait for the item to be copied to the actor
    let item = await super._onDrop(event);
    if (item) { // a proper item was dropped and identified
      if (CONFIG.grpga.testMode) console.debug("Data drop on Actor handled by baseActorSheet");
      return item;
    } else { // a piece of non-item data was dropped
      const dragData = event.dataTransfer.getData("text/plain");
      this.dropData(JSON.parse(dragData))
    }
  }

  /** @override */
  async _onTokenDrop(dragItem) {
    // wait for the item to be copied to the actor
    switch (dragItem.type) {
      case "Item": {
        if (CONFIG.grpga.testMode) console.debug("Data drop on Token handled by baseActorSheet");
        return await super._onTokenDrop(dragItem);
      }
      default: {
        this.dropData(dragItem);
      }
    }
  }

  async _onImportVsDData(event) {
    event.preventDefault();

    // delete unecessary items from the PC Template
    // they are listed in the grpga object in config.js
    let deletions = [];
    for (let trait of this.document.itemTypes.Trait) {
      deletions.push(trait.id);
    }
    for (let extra of grpga.vsd.npcremove) {
      let id = this.actor.data.data.dynamic[extra]?._id;
      if (id) {
        deletions.push(id);
      } else {
        console.debug("Did not find item - ", extra);
      }
    }
    for (let extra of grpga.vsd.npcremovemods) {
      let id = this.actor.data.data.modifiers[extra]?._id;
      if (id) {
        deletions.push(id);
      } else {
        console.debug("Did not find item - ", extra);
      }
    }
    const deleted = await this.actor.deleteEmbeddedDocuments("Item", deletions);

    // start creating the NPC Mods item data
    let nextEntry = 1;
    let itemdata = {
      name: "NPC Mods",
      type: "Modifier",
      data: {
        notes: "The modifiers from the data in the stat block for this creature.",
        chartype: "CharacterVsD",
        inEffect: true,
        alwaysOn: true,
        attack: false,
        defence: false,
        skill: false,
        spell: false,
        check: false,
        reaction: false,
        damage: false,
        primary: false,
        entries: {
          0: {
            value: 0,
            formula: "",
            category: "",
            targets: ""
          }
        }
      }
    }

    const scriptdata = this.actor.data.data.itemscript.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.data.data.itemscript;

    for (let entry of scriptdata) {
      const line = entry.split(":");
      switch (line[0].trim()) {
        case "Name": {
          await this.actor.update({ 'name': line[1].trim() });
          break;
        }
        case "Level": {
          let temp = Number(line[1]);
          let data = {
            name: `Ranks: Level ${temp}`,
            type: "Variable",
            data: {
              notes: entry.trim(),
              chartype: "CharacterVsD",
              value: temp,
              formula: `${temp}`,
              label: "Level",
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  label: ""
                },
                1: {
                  value: temp,
                  formula: `${temp}`,
                  label: "Level"
                }
              }
            }
          };
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "ArmourType": { // Variable
          let temp = line[1].trim();
          let form = "";
          let atype = "";
          switch (temp[0]) {
            case "N":
              form = "NA";
              atype = "No Armor";
              break;
            case "L":
              form = "LA";
              atype = "Light Armor";
              break;
            case "M":
              form = "MA";
              atype = "Medium Armor";
              break;
            case "H":
              form = "HA";
              atype = "Heavy Armor";
              break;
          };
          atype += temp.length == 3 ? " with Shield" : "";
          let data = {
            name: "Armor Type",
            type: "Variable",
            data: {
              chartype: "CharacterVsD",
              value: 0,
              formula: form,
              label: atype,
              notes: entry.trim(),
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  label: ""
                },
                1: {
                  value: 0,
                  formula: form,
                  label: atype
                },
              }
            }
          };
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Defence": { // reaction
          const strvalue = line[1].trim();
          itemdata.data.entries[nextEntry++] = {
            value: Number(strvalue),
            formula: strvalue,
            category: "reaction",
            targets: "Mel, Mis"
          }
          itemdata.data.reaction = true;
          break;
        }
        case "Toughness": { // defence
          let strvalue = Number(line[1].trim()) - this.actor.data.data.dynamic.level.value;
          itemdata.data.entries[nextEntry++] = {
            value: strvalue,
            formula: `${strvalue}`,
            category: "defence",
            targets: "Tough"
          }
          itemdata.data.defence = true;
          break;
        };
        case "Willpower": { // defence
          let strvalue = Number(line[1].trim()) - this.actor.data.data.dynamic.level.value;
          itemdata.data.entries[nextEntry++] = {
            value: strvalue,
            formula: `${strvalue}`,
            category: "defence",
            targets: "Will"
          }
          itemdata.data.defence = true;
          break;
        }
        case "HitPoints": { // Pool
          let temp = Number(line[1]);
          let data = {
            name: "Hit Points",
            type: "Pool",
            data: {
              notes: entry.trim(),
              chartype: "CharacterVsD",
              abbr: "HP",
              name: "Hit Points",
              state: "[H]",
              min: 0,
              value: temp,
              max: temp
            }
          };
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Attack": { // Melee-Attack / Ranged-Attack
          let result = line[1].split("###").map(word => word.trim());
          let formula = result[0];
          if (formula == "0") break;
          let name = "", attacktype = "", attacksize = "";
          if (result[2]) { //there is a name/type in [2] and an attack size in [1]
            name = result[2];
            attacktype = result[2].split(" ")[0].trim();
            attacksize = result[1];
            switch (attacksize) {
              case "Ranged":
              case "Weapon": {
                attacktype = attacksize;
              }
            }
          } else { // there is a name/type in [1]
            name = result[1];
            attacktype = result[1].split(" ")[0].trim();
            attacksize = "Medium";
          }

          let data = {}, attacktable = "", crittable = "", secondcrit = "", maxresult = 0;
          // determine attack and critical tables
          switch (attacktype) {
            case "Beak":
            case "Pincer": {
              attacktable = `${game.i18n.localize(`grpga.tables.beasta`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.cut`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.pierce`)}`;
              break;
            }
            case "Bite": {
              attacktable = `${game.i18n.localize(`grpga.tables.beasta`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.cut`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.impact`)}`;
              break;
            }
            case "Claw":
            case "Talon": {
              attacktable = `${game.i18n.localize(`grpga.tables.beasta`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.cut`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.pierce`)}`;
              break;
            }
            case "Horn":
            case "Tusk":
            case "Spike":
            case "Stinger": {
              attacktable = `${game.i18n.localize(`grpga.tables.beasta`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.pierce`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.none`)}`;
              break;
            }
            case "Grapple":
            case "Envelop":
            case "Swallow": {
              attacktable = `${game.i18n.localize(`grpga.tables.unarmed`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.grapple`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.none`)}`;
              break;
            }
            case "Bash":
            case "Ram":
            case "Unarmed": {
              attacktable = `${game.i18n.localize(`grpga.tables.unarmed`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.impact`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.none`)}`;
              break;
            }
            case "Stomp":
            case "Trample": {
              attacktable = `${game.i18n.localize(`grpga.tables.unarmed`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.impact`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.impact`)}`;
              break;
            }
            case "Ranged": {
              attacktable = `${game.i18n.localize(`grpga.tables.missile`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.pierce`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.none`)}`;
              break;
            }
            default: { // including Weapon
              attacktable = `${game.i18n.localize(`grpga.tables.edged`)}`;
              crittable = `${game.i18n.localize(`grpga.tables.cut`)}`;
              secondcrit = `${game.i18n.localize(`grpga.tables.none`)}`;
            }
          }
          // determine max result
          switch (attacksize) {
            case "Small": {
              switch (attacktype) {
                case "Bite":
                case "Claw":
                case "Talon":
                case "Bash":
                case "Ram":
                case "Unarmed": {
                  maxresult = 90;
                  break;
                }
                default: {
                  maxresult = 80;
                }
              }
              break;
            }
            case "Medium": {
              switch (attacktype) {
                case "Beak":
                case "Pincer":
                case "Grapple":
                case "Envelop":
                case "Swallow": {
                  maxresult = 110;
                  break;
                }
                default: {
                  maxresult = 120;
                }
              }
              break;
            }
            case "Large": {
              switch (name) {
                case "Beak":
                case "Pincer":
                case "Grapple":
                case "Envelop":
                case "Swallow": {
                  maxresult = 130;
                  break;
                }
                default: {
                  maxresult = 140;
                }
              }
              break;
            }
            case "Colossal": {
              maxresult = 175;
              break;
            }
            default: { // including Huge
              maxresult = 150;
            }
          }
          switch (attacktype) {
            case "Ranged": {
              data = {
                name: name,
                type: "Ranged-Attack",
                data: {
                  notes: `${entry.replace(/ ###/g, "").trim()} - Secondary Crit: ${secondcrit}`,
                  chartype: "CharacterVsD",
                  formula: "0",
                  damage: attacktable,
                  damageType: crittable,
                  armourDiv: 4,
                  minST: "oe",
                  accuracy: maxresult,
                  range: "30",
                  rof: 5,
                  bulk: 0,
                }
              }
              itemdata.data.entries[nextEntry++] = {
                value: Number(formula),
                formula: formula,
                category: "attack",
                targets: "Ranged"
              }
              itemdata.data.attack = true;
              break;
            }
            default: { // all others are Melee-Attacks
              data = {
                name: name,
                type: "Melee-Attack",
                data: {
                  notes: `${entry.replace(/ ###/g, "").trim()} - Secondary Crit: ${secondcrit}`,
                  chartype: "CharacterVsD",
                  formula: "0",
                  damage: attacktable,
                  damageType: crittable,
                  armourDiv: 2, // clumsy range
                  minST: "oe", // roll type
                  weight: maxresult, // max result
                  reach: 5 // oerange
                }
              }
              itemdata.data.entries[nextEntry++] = {
                value: Number(formula),
                formula: formula,
                category: "attack",
                targets: name
              }
              itemdata.data.attack = true;
            }
          }
          let critdata = {
            name: `Critical (${attacktype} - Pri)`,
            type: "Defence",
            data: {
              category: "dodge",
              chartype: "CharacterVsD",
              formula: "0",
              notes: crittable,
              value: 0
            }
          }
          await this.actor.createEmbeddedDocuments('Item', [critdata]);
          if (secondcrit != `${game.i18n.localize(`grpga.tables.none`)}`) {
            critdata = {
              name: `Critical (${attacktype} - Sec)`,
              type: "Defence",
              data: {
                category: "dodge",
                chartype: "CharacterVsD",
                formula: "0",
                notes: secondcrit,
                value: 0
              }
            }
            await this.actor.createEmbeddedDocuments('Item', [critdata]);
          }
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "CreatureType": { // maybe just a note but crit mods
          let temp = line[1].trim();
          let scale = "";
          switch (temp[0]) {
            case "N":
              scale = "Normal";
              break;
            case "H":
              scale = "Heroic";
              break;
            case "E":
              scale = "Epic";
              break;
          }
          let data = {
            name: "Creature Type",
            type: "Rollable",
            data: {
              notes: `${scale} ${temp[1] == "H" ? " Humanoid" : " Beast"}`,
              chartype: "CharacterVsD",
              formula: 0,
              category: "check"
            }
          };
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Roguery": {
          const strvalue = line[1].trim();
          itemdata.data.entries[nextEntry++] = {
            value: Number(strvalue),
            formula: strvalue,
            category: "skill",
            targets: "Acr, Ste, Loc, Per, Dec"
          }
          itemdata.data.skill = true;
          break;
        }
        case "Adventuring": {
          const strvalue = line[1].trim();
          itemdata.data.entries[nextEntry++] = {
            value: Number(strvalue),
            formula: strvalue,
            category: "skill",
            targets: "Ath, Rid, Pil, Hun, Nat, Wan"
          }
          itemdata.data.skill = true;
          break;
        }
        case "Lore": {
          const strvalue = line[1].trim();
          itemdata.data.entries[nextEntry++] = {
            value: Number(strvalue),
            formula: strvalue,
            category: "skill",
            targets: "Arc, Cha, Cul, Hea, Son"
          }
          itemdata.data.skill = true;
          break;
        }
        case "MoveRate": { // Variable
          let moves = [];
          if (line[1].includes(" or ")) {
            moves = line[1].split(" or ");
          } else {
            moves = line[1].split("/");
          }
          let nummoves = moves.length;
          let land = 0, air = 0, sea = 0;
          for (let i = 0; i < nummoves; i++) {
            let term = moves[i];
            switch (term[term.length - 1]) {
              case "L": land = Number(term); break;
              case "F": air = Number(term); break;
              case "S": sea = Number(term); break;
            }
          }
          let data = {
            name: "Move Mode",
            type: "Variable",
            data: {
              notes: entry.trim(),
              chartype: "CharacterVsD",
              value: land,
              formula: `${land}`,
              label: "Land Movement",
              entries: {
                0: {
                  value: null,
                  formula: "",
                  label: ""
                },
                1: {
                  value: land,
                  formula: `${land}`,
                  label: "Land Movement"
                },
                2: {
                  value: air,
                  formula: `${air}`,
                  label: "Flying Movement"
                },
                3: {
                  value: sea,
                  formula: `${sea}`,
                  label: "Swimming Movement"
                }
              }
            }
          }
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
      }
    }
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    await this.actor.update({ 'data.biography': biography });
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("Stat block import complete");
  }

  async _onRoll(event) {
    event.preventDefault();
    game.settings.set("grpga", "currentActor", this.actor.id);
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.vsd.${dataset.type}`, { name: dataset.name });
    let unmodified = dataset.threat || 0;
    let openEndedRange = Number(dataset.oerange) || 5;
    let rollValue = Number(dataset.roll);
    let rollType = dataset.rolltype || "oe";
    let critRoll = 0; // for possible critical roll

    // get the modifiers
    let modList = this.fetchRelevantModifiers(this.actor.data, dataset);
    // process the modifiers
    let modformula = "";
    flavour += (dataset.type != "modlist") ? ` [<b>${rollValue}</b>]` : `<p class="chatmod">[<b>${rollValue}</b>]: ${dataset.name}<br>`;
    let hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (dataset.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = (mod.modifier > -1) ? "+" : "";
        modformula += sign + mod.modifier;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    // reaction is actually 
    let formula = (dataset.type == "reaction" || dataset.type == "modlist") ? rollValue : "1d100 + " + rollValue;

    // process the original roll
    let roll = await new Roll(formula + modformula).evaluate({ async: true });
    let firstRoll = roll.terms[0].total;

    switch (dataset.type) {
      case "dodge": { // Standard D100 used for straight table rolls (probably crit or failure)

        // The attacktable will be defined in the item.data.notes field

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`grpga.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${dataset.attacktable}">${game.i18n.localize(`grpga.rolls.table`)}</a></p>`;

        try {
          Journal._showEntry(game.journal.getName(dataset.attacktable).uuid, "text", true);
        } catch (err) {
          if (dataset.attacktable) ui.notifications.error(`There is no matching Journal Entry for ${dataset.attacktable}. Have you imported it from the compendium?`);
        }
        break;
      }
      case "reaction": { // For Melee or Missile Defensive Bonus "rolls"
        // the calculations are made in prepchardata so keep this here for output to chat only
        break;
      }
      case "block": { // Toughness or Willpower Save Roll, open-ended (no crit)
        dataset.attacktable = `${game.i18n.localize(`grpga.tables.fear`)}`;

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`grpga.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${dataset.attacktable}">${game.i18n.localize(`grpga.rolls.saves.fear`)}</a></p>`;

        let newform = "";
        // does it explode
        if (firstRoll <= openEndedRange) { // explodes downward
          newform += `${firstRoll}+${rollValue}${modformula}-1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critfail">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        } else if (firstRoll > 100 - openEndedRange) { // explodes upward
          newform += `${firstRoll}+${rollValue}${modformula}+1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critsuccess">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        }

        if (roll.total < 0) {
          flavour += `<p class="critfail">[<b>${game.i18n.localize(`grpga.rolls.failure`)}</b>]</p>`;
        } else {
          flavour += `<p class="critsuccess">[<b>${game.i18n.localize(`grpga.rolls.success`)}</b>]</p>`;
        }
        break;
      }
      case "skill": { // A skill roll, open-ended (no crit)

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`grpga.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${dataset.attacktable}">${game.i18n.localize(`grpga.rolls.skill.art`)}</a></p>`;

        let newform = "";
        // does it explode
        if (firstRoll <= openEndedRange) { // explodes downward
          newform += `${firstRoll}+${rollValue}${modformula}-1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critfail">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        } else if (firstRoll > 100 - openEndedRange) { // explodes upward
          newform += `${firstRoll}+${rollValue}${modformula}+1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critsuccess">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        }

        if (roll.total < 5) {
          flavour += `<p class="critfail">[<b>${game.i18n.localize(`grpga.rolls.critfail`)}</b>]</p>`;
        } else if (roll.total < 75) {
          flavour += `<p class="critfail">[<b>${game.i18n.localize(`grpga.rolls.failure`)}</b>]</p>`;
        } else if (roll.total < 100) {
          flavour += `<p class="critsuccess">[<b>${game.i18n.localize(`grpga.rolls.partial`)}</b>]</p>`;
        } else if (roll.total < 175) {
          flavour += `<p class="critsuccess">[<b>${game.i18n.localize(`grpga.rolls.success`)}</b>]</p>`;
        } else {
          flavour += `<p class="critsuccess">[<b>${game.i18n.localize(`grpga.rolls.outstanding`)}</b>]</p>`;
        }
        break;
      }
      case "spell": { // A spell roll, open-ended (no crit)

        if (firstRoll % 11 == 0) {
          // Magical Resonance has occurred notify the Caster
          flavour += `<p class="critfail">${game.i18n.localize(`grpga.rolls.resonance`)}</p>`;
        }

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`grpga.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${dataset.journal}">${game.i18n.localize(`grpga.rolls.spell.lore`)}</a>, `;
        flavour += `<a class="open-journal" data-table="${dataset.attacktable}">${game.i18n.localize(`grpga.rolls.spell.sct`)}</a></p>`;

        let newform = "";
        // does it explode
        if (firstRoll <= openEndedRange) { // explodes downward
          newform += `${firstRoll}+${rollValue}${modformula}-1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critfail">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        } else if (firstRoll > 100 - openEndedRange) { // explodes upward
          newform += `${firstRoll}+${rollValue}${modformula}+1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critsuccess">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        }

        try {
          Journal._showEntry(game.journal.getName(dataset.attacktable).uuid, "text", true);
        } catch (err) {
          if (dataset.attacktable) ui.notifications.error(`There is no matching Journal Entry for ${dataset.attacktable}. Have you imported it from the compendium?`);
        }
        break;
      }
      case "attack": { // An attack roll, open-ended (crit)
        // prepare the crit roll for attacks only
        critRoll = await new Roll("1d100").evaluate({ async: true });
        if (firstRoll <= unmodified) {
          // fumble has occurred
          // render roll and critRoll with fumble message then return
          flavour += `<p class="critfail">${game.i18n.localize(`grpga.rolls.d100roll`)}: [<b>${firstRoll} - ${game.i18n.localize(`grpga.rolls.critfail`)}</b>]</p>`;
          if (this.actor.data.data.autoCrit) {
            flavour += `<p>${game.i18n.localize(`grpga.rolls.attack.d100critroll`)}: [<b>${critRoll.total}</b>]</p>`;
          }
          roll.toMessage(
            {
              speaker: ChatMessage.getSpeaker({ actor: this.actor }),
              flavor: flavour
            });
          this.actor.update({ ["data.gmod.value"]: 0 });
          return;
        } else if (unmodified == 10 && firstRoll % 11 == 0) { // only spell attacks have a clumsy range of 10
          // Magical Resonance has occurred on this spell attack. Notify the Caster
          flavour += `<p class="critfail">${game.i18n.localize(`grpga.rolls.resonance`)}</p>`;
        }

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`grpga.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${dataset.attacktable}">${game.i18n.localize(`grpga.rolls.attack.attack`)}</a></p>`;

        let newform = "";
        // does it explode
        if (firstRoll <= openEndedRange) { // explodes downward
          newform += `${firstRoll}+${rollValue}${modformula}-1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critfail">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        } else if (firstRoll > 100 - openEndedRange) { // explodes upward
          newform += `${firstRoll}+${rollValue}${modformula}+1d100x>95`;
          roll = await new Roll(newform).evaluate({ async: true });
          flavour += `<p class="critsuccess">${rollType.toUpperCase()} ${game.i18n.localize(`grpga.rolls.roll`)}: [<b>${roll.dice[0].total}</b>]</p>`;
        }

        // display the automated crit roll if desired
        if (this.actor.data.data.autoCrit) {
          flavour += `<p>${game.i18n.localize(`grpga.rolls.attack.d100critroll`)}: [<b>${critRoll.total}</b>] - `;
          flavour += `<a class="open-journal" data-table="${dataset.crittable}">${game.i18n.localize(`grpga.rolls.attack.critical`)}</a></p>`;
        }
        try {
          Journal._showEntry(game.journal.getName(dataset.attacktable).uuid, "text", true);
        } catch (err) {
          if (dataset.attacktable) ui.notifications.error(`There is no matching Journal Entry for ${dataset.attacktable}. Have you imported it from the compendium?`);
        }
        if (roll.total > dataset.maxresult) {
          flavour += `<p class="critfail">${game.i18n.localize(`grpga.rolls.attack.maxresult`)}: [<b>${dataset.maxresult}</b>]</p>`;
        }
        break;
      }
      case "modlist": { // all the work has been done above. Maybe move it in here to tidy up?
        flavour += `<hr><p>${roll.result} = <b>${roll.total}</b></p>`;
        new Dialog({
          title: this.actor.name,
          content: flavour,
          buttons: {
            close: {
              icon: "<i class='fas fa-tick'></i>",
              label: "Close"
            },
          },
          default: "close"
        }).render(true)
        return;
      }
      default: { // unsupported roll types
        ui.notifications.info(`The ${dataset.type} roll is not yet supported.`);
        return;
      }
    }

    // prepare the flavor in advance based on which type of die is in use.
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour
      }
    );
    this.actor.update({ ["data.gmod.value"]: 0 });
    return;
  }
}
