import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class Actor3D6Sheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actor3d6-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importGCS').click(this._onImportGCSData.bind(this));
  }

  async _onImportGCSData(event) {
    event.preventDefault();
    const scriptdata = this.actor.data.data.itemscript.split(/\r?\n/).map(word => word.trim());
    const useFAHR = this.actor.data.data.useFAHR;
    let race = "Human";

    for (let entry of scriptdata) {
      let regex = new RegExp(/([A-z- ]+):([^\r|\n]+)/gi);
      let line = regex.exec(entry);
      switch (line[1].trim()) {
        case "Name": {
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Spell":
        case "Skill": {
          let data = {
            type: "Rollable",
            data: {
              chartype: "Character3D6",
              category: line[1].toLowerCase(),
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          let regex = new RegExp(/([^ +-]+)?([+|-][0-9]+)/gi);
          let rsl = regex.exec(result[1]);
          if (!rsl) {
            data.name = result[0];
          } else if (rsl[1]) {
            data.name = result[0];
            // FIXME: need to slugify rsl[1] perhaps to be safe
            data.data.formula = `@${this.actor.slugify(rsl[1])} ${rsl[2]}`;
          } else { // technique or rms
            let diff = rsl[2];
            regex = new RegExp(/([A-Z ]+) \(([^\)]+\)*)\)/gi);
            rsl = regex.exec(result[0]);
            data.name = rsl[0];
            // FIXME: need to slugify rsl[2]
            data.data.formula = `@${this.actor.slugify(rsl[2])} ${diff}`;
          }
          data.data.notes = result[2];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Primary-Attribute": {
          let data = {
            type: line[1],
            data: {
              chartype: "Character3D6"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1];
          data.data.attr = result[2];
          // look to see if we already have an attribute with this name
          const item = this.actor.data.items.find(i => i.name == result[0] && i.type == "Primary-Attribute");
          if (item?.id) {
            await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.attr': result[2] }]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Pool": {
          let data = {
            type: line[1],
            data: {
              chartype: "Character3D6"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.abbr = result[1];
          data.data.max = result[2];
          data.data.value = result[2];
          // look to see if we already have a pool with this name
          const item = this.actor.data.items.find(i => i.name == result[0]);
          if (item?.id) {
            await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'data.max': result[2], 'data.value': result[2] }]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Basic Speed": {
          await this.actor.update({ 'data.bs.value': Number(line[2]) });
          break;
        }
        case "Basic Move": {
          await this.actor.update({ 'data.bm.move': Number(line[2]) });
          break;
        }
        case "Check": {
          let data = {
            type: "Rollable",
            data: {
              chartype: "Character3D6",
              category: line[1].toLowerCase(),
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Melee-Attack": {
          let data = {
            type: line[1],
            data: {
              chartype: "Character3D6",
              armourDiv: 1
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1];
          let damage = result[2].split(" ").map(word => word.trim());
          data.data.damage = damage[0].replace('d', 'd6');
          data.data.damageType = damage[1];
          data.data.minST = result[3];
          data.data.weight = result[4];
          data.data.reach = result[5];
          data.data.notes = result[6];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Defence": {
          let data = {
            type: line[1],
            data: {
              chartype: "Character3D6",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = ((result[1] == "No") ? "Block - " : "Parry - ") + result[0];
          data.data.formula = ((result[1] == "No") ? result[2] : result[1].split("U")[0]);
          data.data.weight = result[3];
          data.data.notes = result[4];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Ranged-Attack": {
          let data = {
            type: line[1],
            data: {
              chartype: "Character3D6",
              armourDiv: 1
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          data.data.formula = result[1];
          let damage = result[2].split(" ").map(word => word.trim());
          data.data.damage = damage[0].replace('d', 'd6');
          data.data.damageType = damage[1];
          data.data.minST = result[3];
          data.data.accuracy = result[4];
          data.data.range = result[5];
          data.data.rof = result[6];
          data.data.shots = result[7];
          data.data.bulk = result[8];
          data.data.recoil = result[9];
          data.data.notes = result[10];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Advantage":
        case "Disadvantage":
        case "Perk":
        case "Quirk": {
          let data = {
            type: "Trait",
            data: {
              chartype: "Character3D6",
              category: line[1].toLowerCase(),
              notes: ""
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          data.name = result[0];
          if (result[1]) data.data.notes += 'Modifier: ' + result[1] + '\r\n';
          data.data.notes += result[2];
          if (result[3] == "RACE") race = result[0];
          await this.actor.createEmbeddedDocuments('Item', [data]);
          break;
        }
        case "Hit-Location": {
          if (!useFAHR) {
            let data = {
              type: line[1],
              data: {
                chartype: "Character3D6",
              }
            };
            let result = line[2].split("###").map(word => word.trim());
            data.name = result[0];
            data.data.damageResistance = result[1];
            data.data.toHitPenalty = result[2];
            data.data.notes = result[3];
            data.data.toHitRoll18 = result[4];
            await this.actor.createEmbeddedDocuments('Item', [data]);
          }
          break;
        }
        case "Equipment": {
          let result = line[2].split("###").map(word => word.trim());
          switch (result[4].trim()) {
            case "Body Armour": {
              let data = {
                type: "Hit-Location",
                data: {
                  chartype: "Character3D6",
                  hp: this.actor.data.data.tracked.hp.value,
                  notes: result[0] + "; " + result[7]
                }
              };
              let regex = new RegExp(/^DR:([\d]+)/gi);
              data.data.damageResistance = Number(regex.exec(result[7])[1]);
              regex = new RegExp(/locn:([\w ]+)/gi);
              let locations = Array.from(result[6].matchAll(regex));
              for (let i = 0; i < locations.length; i++) {
                switch (locations[i][1].trim()) {
                  case "fullbody": {
                    await this.head(data);
                    await this.neck(data);
                    await this.torso(data);
                    await this.arms(data);
                    await this.legs(data);
                    await this.hands(data);
                    await this.feet(data);
                    break;
                  }
                  case "head": {
                    await this.head(data);
                    break;
                  }
                  case "skull": {
                    await this.skull(data);
                    break;
                  }
                  case "face": {
                    await this.face(data);
                    break;
                  }
                  case "neck": {
                    await this.neck(data);
                    break;
                  }
                  case "torso": {
                    await this.torso(data);
                    break;
                  }
                  case "chest": {
                    await this.chest(data);
                    break;
                  }
                  case "abdomen": {
                    await this.abdomen(data);
                    break;
                  }
                  case "vitals": {
                    await this.vitals(data);
                    break;
                  }
                  case "arms": {
                    await this.arms(data);
                    break;
                  }
                  case "shoulders": {
                    await this.shoulders(data);
                    break;
                  }
                  case "upperarms": {
                    await this.upperarms(data);
                    break;
                  }
                  case "elbows": {
                    await this.elbows(data);
                    break;
                  }
                  case "forearms": {
                    await this.forearms(data);
                    break;
                  }
                  case "hands": {
                    await this.hands(data);
                    break;
                  }
                  case "legs": {
                    await this.legs(data);
                    break;
                  }
                  case "thighs": {
                    await this.thighs(data);
                    break;
                  }
                  case "knees": {
                    await this.knees(data);
                    break;
                  }
                  case "shins": {
                    await this.shins(data);
                    break;
                  }
                  case "feet": {
                    await this.feet(data);
                    break;
                  }
                  case "eyes": {
                    await this.eyes(data);
                    break;
                  }
                  case "groin": {
                    await this.groin(data);
                    break;
                  }
                }
              }
            }
          }
          break;
        }
        default: {
          // append the entry to notes
        }
      }
    }
    await this.actor.update({ 'data.itemscript': '' });
    ui.notifications.info("GCS import complete");
  }


  /**
  * Handle clickable rolls.
  * @param {Event} event   The originating click event
  * @private
  */
  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.3d6.${dataset.type}`, { name: dataset.name });
    let isDamageOrReaction = false;

    var formula = "";
    switch (dataset.type) {
      case "damage": {
        formula = dataset.roll;
        flavour += ` [<b>${dataset.roll} ${dataset.rolltype} (${dataset.armourdiv})</b>]`;
        isDamageOrReaction = true;
        break;
      };
      case "reaction": {
        formula = "3d6";
        isDamageOrReaction = true;
        break;
      };
      default: {
        formula = dataset.roll;
        flavour += ` [<b>${dataset.roll}</b>]`;
        break;
      }
    }

    var modList = this.fetchRelevantModifiers(this.actor.data, dataset);

    var hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += `<p class="chatmod">`;
      for (const mod of modList) {
        sign = (mod.modifier > -1) ? "+" : "";
        formula += sign + mod.modifier;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    let r = await new Roll(formula).evaluate({async: true});

    if (hasMods || isDamageOrReaction) {
      flavour += `<hr><p class="">${r.formula} = [<b>${r.total}</b>]`;
    }

    let r3d6 = await new Roll("3d6").evaluate({async: true});

    // a Success roll
    if (!isDamageOrReaction) {
      let measureofsuccess = r.total - r3d6.total;
      let success = measureofsuccess > -1;
      let critfail = false;
      let critsuccess = false;
      switch (r3d6.total) {
        case 3:
        case 4: {
          critsuccess = true;
          break;
        }
        case 5:
        case 6: {
          critsuccess = measureofsuccess > 9;
          break;
        }
        case 17: {
          success = false;
          critfail = measureofsuccess < -1;
          break;
        }
        case 18: {
          success = !(critfail = true);
          break;
        }
      }
      if (!hasMods) {
        flavour += `<hr><p class="">`;
      } else {
        flavour += `<br>`;
      }
      flavour += `3D6 Roll: [<b>${r3d6.total}</b>] <i class="fas fa-arrow-right"></i> <span class="`;
      if (critsuccess) flavour += `critsuccess">Critical Success`;
      else if (critfail) flavour += `critfail">Critical Failure`;
      else if (success) flavour += `success">Success by ${measureofsuccess}`;
      else flavour += `failure">Failure by ${-measureofsuccess}`;
      flavour += `</span></p>`
    }

    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour,
        flags: { hideMessageContent: true }
      }
    );

    this.actor.update({ ["data.gmod.value"]: 0 });
    return;
  }

  async head(data) {
    this.skull(data);
    this.face(data);
    this.eyes(data);
  }
  async skull(data) {
    data.name = "Skull";
    data.data.toHitRoll18 = "3,4";
    data.data.toHitRoll6 = "All";
    data.data.toHitPenalty = -7;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async face(data) {
    data.name = "Face";
    data.data.toHitRoll18 = "5";
    data.data.toHitRoll6 = "All";
    data.data.toHitPenalty = -5;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async eyes(data) {
    data.name = "Eyes";
    data.data.toHitRoll18 = "-";
    data.data.toHitRoll6 = "-";
    data.data.toHitPenalty = -9;
    data.data.toCripple = Number(data.data.hp / 10);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async neck(data) {
    data.name = "Neck";
    data.data.toHitRoll18 = "17,18";
    data.data.toHitRoll6 = "All";
    data.data.toHitPenalty = -5;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async torso(data) {
    this.chest(data);
    this.abdomen(data);
    this.vitals(data);
  }
  async chest(data) {
    data.name = "Chest";
    data.data.toHitRoll18 = "9,10";
    data.data.toHitRoll6 = "2,3,4,5,6";
    data.data.toHitPenalty = -0;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async abdomen(data) {
    data.name = "Abdomen";
    data.data.toHitRoll18 = "11";
    data.data.toHitRoll6 = "2,3,4,5,6";
    data.data.toHitPenalty = -2;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async vitals(data) {
    data.name = "Vitals";
    data.data.toHitRoll18 = "9,10,11";
    data.data.toHitRoll6 = "1";
    data.data.toHitPenalty = -3;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async groin(data) {
    data.name = "Groin";
    data.data.toHitRoll18 = "-";
    data.data.toHitRoll6 = "-";
    data.data.toHitPenalty = -3;
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async arms(data) {
    this.shoulders(data);
    this.upperarms(data);
    this.elbows(data);
    this.forearms(data);
  }
  async shoulders(data) {
    data.name = "Right Shoulder";
    data.data.toHitRoll18 = "8";
    data.data.toHitRoll6 = "6";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Shoulder";
    data.data.toHitRoll18 = "12";
    data.data.toHitRoll6 = "6";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async upperarms(data) {
    data.name = "Right Upper Arm";
    data.data.toHitRoll18 = "8";
    data.data.toHitRoll6 = "5";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Upper Arm";
    data.data.toHitRoll18 = "12";
    data.data.toHitRoll6 = "5";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async elbows(data) {
    data.name = "Right Elbow";
    data.data.toHitRoll18 = "8";
    data.data.toHitRoll6 = "4";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Elbow";
    data.data.toHitRoll18 = "12";
    data.data.toHitRoll6 = "4";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async forearms(data) {
    data.name = "Right Forearm";
    data.data.toHitRoll18 = "8";
    data.data.toHitRoll6 = "1,2,3";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Forearm";
    data.data.toHitRoll18 = "12";
    data.data.toHitRoll6 = "1,2,3";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async hands(data) {
    data.name = "Right Hand";
    data.data.toHitRoll18 = "15";
    data.data.toHitRoll6 = "1,3,5";
    data.data.toHitPenalty = -4;
    data.data.toCripple = Number(data.data.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Hand";
    data.data.toHitRoll18 = "15";
    data.data.toHitRoll6 = "2,4,6";
    data.data.toHitPenalty = -4;
    data.data.toCripple = Number(data.data.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async legs(data) {
    this.thighs(data);
    this.knees(data);
    this.shins(data);
  }
  async thighs(data) {
    data.name = "Right Thigh";
    data.data.toHitRoll18 = "6,7";
    data.data.toHitRoll6 = "5,6";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Thigh";
    data.data.toHitRoll18 = "13,14";
    data.data.toHitRoll6 = "5,6";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async knees(data) {
    data.name = "Right Knee";
    data.data.toHitRoll18 = "6,7";
    data.data.toHitRoll6 = "4";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Knee";
    data.data.toHitRoll18 = "13,14";
    data.data.toHitRoll6 = "4";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async shins(data) {
    data.name = "Right Shin";
    data.data.toHitRoll18 = "6,7";
    data.data.toHitRoll6 = "1,2,3";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Shin";
    data.data.toHitRoll18 = "13,14";
    data.data.toHitRoll6 = "1,2,3";
    data.data.toHitPenalty = -2;
    data.data.toCripple = Number(data.data.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
  async feet(data) {
    data.name = "Right Foot";
    data.data.toHitRoll18 = "16";
    data.data.toHitRoll6 = "1,3,5";
    data.data.toHitPenalty = -4;
    data.data.toCripple = Number(data.data.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [data]);
    data.name = "Left Foot";
    data.data.toHitRoll18 = "16";
    data.data.toHitRoll6 = "2,4,6";
    data.data.toHitPenalty = -4;
    data.data.toCripple = Number(data.data.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [data]);
  }
}
