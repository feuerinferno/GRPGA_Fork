import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorGRPSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorGRP-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /**
  * Handle clickable rolls.
  * @param {Event} event   The originating click event
  * @private
  */
  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    let flavour = game.i18n.format(`grpga.phrases.3d6.${dataset.type}`, { name: dataset.name });
    let isDamageOrReaction = false;

    var formula = "";
    switch (dataset.type) {
      case "damage": {
        formula = dataset.roll;
        flavour += ` [<b>${dataset.roll} ${dataset.rolltype} (${dataset.armourdiv})</b>]`;
        isDamageOrReaction = true;
        break;
      };
      case "reaction": {
        formula = "3d6";
        isDamageOrReaction = true;
        break;
      };
      default: {
        formula = dataset.roll;
        flavour += ` [<b>${dataset.roll}</b>]`;
        break;
      }
    }

    var modList = this.fetchRelevantModifiers(this.actor.data, dataset);

    var hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += `<p class="chatmod">`;
      for (const mod of modList) {
        sign = (mod.modifier > -1) ? "+" : "";
        formula += sign + mod.modifier;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    let r = await new Roll(formula).evaluate({async: true});

    if (hasMods || isDamageOrReaction) {
      flavour += `<hr><p class="">${r.formula} = [<b>${r.total}</b>]`;
    }

    let r3d6 = await new Roll("3d6").evaluate({async: true});

    // a Success roll
    if (!isDamageOrReaction) {
      let measureofsuccess = r.total - r3d6.total;
      let success = measureofsuccess > -1;
      let critfail = false;
      let critsuccess = false;
      switch (r3d6.total) {
        case 3:
        case 4: {
          critsuccess = true;
          break;
        }
        case 5:
        case 6: {
          critsuccess = measureofsuccess > 9;
          break;
        }
        case 17: {
          success = false;
          critfail = measureofsuccess < -1;
          break;
        }
        case 18: {
          success = !(critfail = true);
          break;
        }
      }
      if (!hasMods) {
        flavour += `<hr><p class="">`;
      } else {
        flavour += `<br>`;
      }
      flavour += `3D6 Roll: [<b>${r3d6.total}</b>] <i class="fas fa-arrow-right"></i> <span class="`;
      if (critsuccess) flavour += `critsuccess">Critical Success`;
      else if (critfail) flavour += `critfail">Critical Failure`;
      else if (success) flavour += `success">Success by ${measureofsuccess}`;
      else flavour += `failure">Failure by ${-measureofsuccess}`;
      flavour += `</span></p>`
    }

    r.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: flavour
      }
    );

    this.actor.update({ ["data.gmod.value"]: 0 });
    return;
  }
}
