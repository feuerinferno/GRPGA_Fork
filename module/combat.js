export class VSDCombat extends Combat {

  /**
    * Set initiative for a single Combatant within the Combat encounter.
    * Turns will be updated to set the initiative to the second combatant and then back up one
    * to satisfy modules that hook on next or previous
    * @param {string} id         The combatant ID for which to set initiative
    * @param {number} value      A specific initiative value to set
    */
  async setInitiative(id, value) {
    const combatant = this.combatants.get(id, { strict: true });
    await combatant.update({ id: id, initiative: value }, {});
    let turn = 0;
    if (this.settings.skipDefeated) {
      turn = this.turns.findIndex(t => {
        return !(t.defeated ||
          t.actor?.effects.find(e => e.getFlag("core", "statusId") === CONFIG.Combat.defeatedStatusId));
      }) + 1;
      if (turn === -1) {
        ui.notifications.warn(game.i18n.localize("COMBAT.NoneRemaining"));
        turn = 0;
      }
    }
    await this.update({ turn: turn });
    await this.previousTurn();
  }
}
