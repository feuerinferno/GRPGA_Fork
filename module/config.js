export const grpga = {}

CONFIG.ChatMessage.template = "systems/grpga/templates/chat/chat-message.hbs";

CONFIG.postures = [
    "systems/grpga/icons/postures/standing.png",
    "systems/grpga/icons/postures/sitting.png",
    "systems/grpga/icons/postures/crouching.png",
    "systems/grpga/icons/postures/crawling.png",
    "systems/grpga/icons/postures/kneeling.png",
    "systems/grpga/icons/postures/lyingback.png",
    "systems/grpga/icons/postures/lyingprone.png",
    "systems/grpga/icons/postures/sittingchair.png"
];

CONFIG.sizemods = [
    "systems/grpga/icons/sizemods/smneg1.png",
    "systems/grpga/icons/sizemods/smneg2.png",
    "systems/grpga/icons/sizemods/smneg3.png",
    "systems/grpga/icons/sizemods/smneg4.png",
    "systems/grpga/icons/sizemods/smpos1.png",
    "systems/grpga/icons/sizemods/smpos2.png",
    "systems/grpga/icons/sizemods/smpos3.png",
    "systems/grpga/icons/sizemods/smpos4.png"
];

CONFIG.crippled = [
    "systems/grpga/icons/crippled/crippledleftarm.png",
    "systems/grpga/icons/crippled/crippledlefthand.png",
    "systems/grpga/icons/crippled/crippledleftleg.png",
    "systems/grpga/icons/crippled/crippledleftfoot.png",
    "systems/grpga/icons/crippled/crippledrightarm.png",
    "systems/grpga/icons/crippled/crippledrighthand.png",
    "systems/grpga/icons/crippled/crippledrightleg.png",
    "systems/grpga/icons/crippled/crippledrightfoot.png",
];

CONFIG.statusEffects3d6 = [
    { icon: 'systems/grpga/icons/postures/standing.png', id: 'standing', label: 'grpga.postures.standing' },
    { icon: 'systems/grpga/icons/postures/sitting.png', id: 'sitting', label: 'grpga.postures.sitting' },
    { icon: 'systems/grpga/icons/postures/crouching.png', id: 'crouching', label: 'grpga.postures.crouching' },
    { icon: 'systems/grpga/icons/postures/crawling.png', id: 'crawling', label: 'grpga.postures.crawling' },
    { icon: 'systems/grpga/icons/postures/kneeling.png', id: 'kneeling', label: 'grpga.postures.kneeling' },
    { icon: 'systems/grpga/icons/postures/lyingback.png', id: 'lyingback', label: 'grpga.postures.proneb' },
    { icon: 'systems/grpga/icons/postures/lyingprone.png', id: 'lyingprone', label: 'grpga.postures.pronef' },
    { icon: 'systems/grpga/icons/postures/sittingchair.png', id: 'sittingchair', label: 'grpga.postures.sitting' },
    { icon: 'systems/grpga/icons/conditions/shock1.png', id: 'shock1', label: 'grpga.conditions.shock' },
    { icon: 'systems/grpga/icons/conditions/shock2.png', id: 'shock2', label: 'grpga.conditions.shock' },
    { icon: 'systems/grpga/icons/conditions/shock3.png', id: 'shock3', label: 'grpga.conditions.shock' },
    { icon: 'systems/grpga/icons/conditions/shock4.png', id: 'shock4', label: 'grpga.conditions.shock' },
    { icon: 'systems/grpga/icons/conditions/reeling.png', id: 'reeling', label: 'grpga.conditions.reeling' },
    { icon: 'systems/grpga/icons/conditions/tired.png', id: 'tired', label: 'grpga.conditions.tired' },
    { icon: 'systems/grpga/icons/conditions/collapse.png', id: 'collapse', label: 'grpga.conditions.collapse' },
    { icon: 'systems/grpga/icons/conditions/unconscious.png', id: 'unconscious', label: 'grpga.conditions.unconscious' },
    { icon: 'systems/grpga/icons/conditions/minus1xhp.png', id: 'minushp1', label: 'grpga.conditions.minushp' },
    { icon: 'systems/grpga/icons/conditions/minus2xhp.png', id: 'minushp2', label: 'grpga.conditions.minushp' },
    { icon: 'systems/grpga/icons/conditions/minus3xhp.png', id: 'minushp3', label: 'grpga.conditions.minushp' },
    { icon: 'systems/grpga/icons/conditions/minus4xhp.png', id: 'minushp4', label: 'grpga.conditions.minushp' },
    { icon: 'systems/grpga/icons/conditions/stunned.png', id: 'stunned', label: 'grpga.conditions.stunned' },
    { icon: 'systems/grpga/icons/conditions/surprised.png', id: 'surprised', label: 'grpga.conditions.surprised' },
    { icon: 'systems/grpga/icons/defeated.png', id: 'defeated', label: 'grpga.conditions.defeated' },
    { icon: 'systems/grpga/icons/blank.png', id: 'none', label: 'grpga.conditions.none' },
    { icon: 'systems/grpga/icons/stances/hth.svg', id: 'hth', label: 'grpga.stances.hth' },
    { icon: 'systems/grpga/icons/stances/magic.svg', id: 'magic', label: 'grpga.stances.magic' },
    { icon: 'systems/grpga/icons/stances/bow.svg', id: 'bow', label: 'grpga.stances.bow' },
    { icon: 'systems/grpga/icons/stances/thrown.svg', id: 'thrown', label: 'grpga.stances.thrown' },
    { icon: 'systems/grpga/icons/crippled/crippledleftarm.png', id: 'crippledleftarm', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledlefthand.png', id: 'crippledlefthand', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledleftleg.png', id: 'crippledleftleg', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledleftfoot.png', id: 'crippledleftfoot', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledrightarm.png', id: 'crippledrightarm', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledrighthand.png', id: 'crippledrighthand', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledrightleg.png', id: 'crippledrightleg', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/crippled/crippledrightfoot.png', id: 'crippledrightfoot', label: 'grpga.conditions.crippled' },
    { icon: 'systems/grpga/icons/sizemods/smneg1.png', id: 'smaller1', label: 'grpga.conditions.smaller' },
    { icon: 'systems/grpga/icons/sizemods/smneg2.png', id: 'smaller2', label: 'grpga.conditions.smaller' },
    { icon: 'systems/grpga/icons/sizemods/smneg3.png', id: 'smaller3', label: 'grpga.conditions.smaller' },
    { icon: 'systems/grpga/icons/sizemods/smneg4.png', id: 'smaller4', label: 'grpga.conditions.smaller' },
    { icon: 'systems/grpga/icons/sizemods/smpos1.png', id: 'larger1', label: 'grpga.conditions.larger' },
    { icon: 'systems/grpga/icons/sizemods/smpos2.png', id: 'larger2', label: 'grpga.conditions.larger' },
    { icon: 'systems/grpga/icons/sizemods/smpos3.png', id: 'larger3', label: 'grpga.conditions.larger' },
    { icon: 'systems/grpga/icons/sizemods/smpos4.png', id: 'larger4', label: 'grpga.conditions.larger' }
];
CONFIG.statusEffectsd20 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsgrp = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd120 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd100 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsoats = CONFIG.statusEffects3d6;
CONFIG.statusEffectsvsd = CONFIG.statusEffects3d6;

CONFIG.controlIcons.defeated = "systems/grpga/icons/defeated.png";

CONFIG.JournalEntry.noteIcons = {
    "Marker": "systems/grpga/icons/buildings/point_of_interest.png",
    "Apothecary": "systems/grpga/icons/buildings/apothecary.png",
    "Beastmen Herd 1": "systems/grpga/icons/buildings/beastmen_camp1.png",
    "Beastmen Herd 2": "systems/grpga/icons/buildings/beastmen_camp2.png",
    "Blacksmith": "systems/grpga/icons/buildings/blacksmith.png",
    "Bretonnian City 1": "systems/grpga/icons/buildings/bret_city1.png",
    "Bretonnian City 2": "systems/grpga/icons/buildings/bret_city2.png",
    "Bretonnian City 3": "systems/grpga/icons/buildings/bret_city3.png",
    "Bretonnian Worship": "systems/grpga/icons/buildings/bretonnia_worship.png",
    "Caste Hill 1": "systems/grpga/icons/buildings/castle_hill1.png",
    "Caste Hill 2": "systems/grpga/icons/buildings/castle_hill2.png",
    "Caste Hill 3": "systems/grpga/icons/buildings/castle_hill3.png",
    "Castle Wall": "systems/grpga/icons/buildings/castle_wall.png",
    "Cave 1": "systems/grpga/icons/buildings/cave1.png",
    "Cave 2": "systems/grpga/icons/buildings/cave2.png",
    "Cave 3": "systems/grpga/icons/buildings/cave3.png",
    "Cemetery": "systems/grpga/icons/buildings/cemetery.png",
    "Chaos Portal": "systems/grpga/icons/buildings/chaos_portal.png",
    "Chaos Worship": "systems/grpga/icons/buildings/chaos_worship.png",
    "Court": "systems/grpga/icons/buildings/court.png",
    "Dwarf Beer": "systems/grpga/icons/buildings/dwarf_beer.png",
    "Dwarf Hold 1": "systems/grpga/icons/buildings/dwarf_hold1.png",
    "Dwarf Hold 2": "systems/grpga/icons/buildings/dwarf_hold2.png",
    "Dwarf Hold 3": "systems/grpga/icons/buildings/dwarf_hold3.png",
    "Empire Barracks": "systems/grpga/icons/buildings/empire_barracks.png",
    "Empire City 1": "systems/grpga/icons/buildings/empire_city1.png",
    "Empire City 2": "systems/grpga/icons/buildings/empire_city2.png",
    "Empire City 3": "systems/grpga/icons/buildings/empire_city3.png",
    "Farm": "systems/grpga/icons/buildings/farms.png",
    "Food": "systems/grpga/icons/buildings/food.png",
    "Guard Post": "systems/grpga/icons/buildings/guards.png",
    "Haunted Hill": "systems/grpga/icons/buildings/haunted_hill.png",
    "Haunted Wood": "systems/grpga/icons/buildings/haunted_wood.png",
    "Inn 1": "systems/grpga/icons/buildings/inn1.png",
    "Inn 2": "systems/grpga/icons/buildings/inn2.png",
    "Kislev City 1": "systems/grpga/icons/buildings/kislev_city1.png",
    "Kislev City 2": "systems/grpga/icons/buildings/kislev_city2.png",
    "Kislev City 3": "systems/grpga/icons/buildings/kislev_city3.png",
    "Lumber": "systems/grpga/icons/buildings/lumber.png",
    "Magic": "systems/grpga/icons/buildings/magic.png",
    "Metal": "systems/grpga/icons/buildings/metal.png",
    "Mountain 1": "systems/grpga/icons/buildings/mountains1.png",
    "Mountain 2": "systems/grpga/icons/buildings/mountains2.png",
    "Orcs": "systems/grpga/icons/buildings/orcs.png",
    "Orc Camp": "systems/grpga/icons/buildings/orc_city.png",
    "Port": "systems/grpga/icons/buildings/port.png",
    "Road": "systems/grpga/icons/buildings/roads.png",
    "Ruins": "systems/grpga/icons/buildings/ruins.png",
    "Scroll": "systems/grpga/icons/buildings/scroll.png",
    "Sigmar": "systems/grpga/icons/buildings/sigmar_worship.png",
    "Stables": "systems/grpga/icons/buildings/stables.png",
    "Standing Stones": "systems/grpga/icons/buildings/standing_stones.png",
    "Swamp": "systems/grpga/icons/buildings/swamp.png",
    "Temple": "systems/grpga/icons/buildings/temple.png",
    "Textile": "systems/grpga/icons/buildings/textile.png",
    "Tower 1": "systems/grpga/icons/buildings/tower1.png",
    "Tower 2": "systems/grpga/icons/buildings/tower2.png",
    "Tower Hill": "systems/grpga/icons/buildings/tower_hill.png",
    "Wizard Tower": "systems/grpga/icons/buildings/wizard_tower.png",
    "Ulric": "systems/grpga/icons/buildings/ulric_worship.png",
    "Village 1": "systems/grpga/icons/buildings/village1.png",
    "Village 2": "systems/grpga/icons/buildings/village2.png",
    "Village 3": "systems/grpga/icons/buildings/village3.png",
    "Wood Elves 1": "systems/grpga/icons/buildings/welves1.png",
    "Wood Elves 2": "systems/grpga/icons/buildings/welves2.png",
    "Wood Elves 3": "systems/grpga/icons/buildings/welves3.png"
};
grpga.postures = {
    "standing": "grpga.postures.standing",
    "crouching": "grpga.postures.crouching",
    "kneeling": "grpga.postures.kneeling",
    "crawling": "grpga.postures.crawling",
    "sitting": "grpga.postures.sitting",
    "pronef": "grpga.postures.pronef",
    "proneb": "grpga.postures.proneb"
};
grpga.combat = {
    defaultActionCost: 1,
    defaultShotCost: 3,
    missingInitiative: "You must roll initiative first."
};
grpga.vsd = {
    skillsort: [
        { name: "Combat", data: ["blunt", "blades", "ranged", "polearms", "brawl"] },
        { name: "Adventuring", data: ["athletics", "ride", "hunting", "nature", "wandering"] },
        { name: "Roguery", data: ["acrobatics", "stealth", "locks-traps", "perception", "deceive"] },
        { name: "Lore", data: ["arcana", "charisma", "cultures", "healer", "songs-tales"] }
    ],
    skillvariables: ["difficulty", "helpers"],
    oddskills: ["body", "armor"],
    spellvariables: ["attack-spell-range", "spell-range", "prepaim-time"],
    spellpools: ["edb", "mp", "ah1", "ah3", "ah5", "ra5", "tokens"],
    specialrolls: ["brawl-unarmed-touch", "spell-failure", "magic-resonance-roll"],
    specialmods: ["failed-spell-type", "mrr-location", "mrr-spell-type", "weave", "critical-severity"],
    defencevariables: ["move-mode", "move-rate", "armor-type"],
    defencesort: ["melee-db", "missile-db"],
    savesort: ["willpower-saving-roll", "toughness-saving-roll"],
    defencepools: ["drive", "hero", "hp", "sst"],
    defencemodifiers: ["attack-level"],
    attackpools: ["edb", "parry", "tokens"],
    attackvariables: ["missile-range", "prepaim-time"],
    specialcombatrolls: ["fumble-meleethrown", "fumble-missile"],
    specialcombatmods: ["fumble-mod", "critical-severity"],
    primarysort: {
        name: "Stats",
        data: ["brn", "swi", "for", "wit", "wsd", "bea"]
    },
    advancementpools: ["drive", "hero", "xp", "wl"],
    npcremove: [
        "bea", "brn", "for", "swi", "wsd", "wit",
        "blunt", "blades", "polearms", "ranged", "brawl",
        "magic-resonance-roll", "spell-failure", "armor", "body",
        "blades-dagger-pierce", "brawl-dagger-pierce", "brawl-grappling-grapple", "brawl-kick-impact", "brawl-punch-impact",
        "drive", "xp", "hero", "mp", "wl",
        "armor-type", "attack-spell-range", "helpers", "mrr-location", "mrr-spell-type", "move-mode", "ranks-level-1",
        "spell-range", "weave", "critical-grapple", "critical-impact", "critical-pierce", "critical-cut"
    ],
    npcremovemods: [
        "attack-spell-range", "spell-range", "target-is-static", "failed-spell-type",
        "mrr-location-mod", "mrr-spell-type-mod", "weave-mod",
        "bearing-bonus", "brawn-bonus", "swiftness-bonus", "fortitude-bonus", "wisdom-bonus", "wits-bonus",
        "character-attribute-creation", "drive-bonus", "helpers"
    ]
};

