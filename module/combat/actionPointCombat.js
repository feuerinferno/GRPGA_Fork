import { grpga } from "../config.js";

export class ActionPointCombat extends Combat {

    async startCombat() {
        await this.setupTurns();
        return super.startCombat();
    }

    async nextTurn() {
        let turn = this.turn;
        await this.combatant.update({ ["flags.grpga.actions"]: this.turns[turn].data.flags.grpga.actions-1});

        // Determine the next turn number
        let next = turn;
        let found = false;
        // find a non-defeated turn after this one with at least one action remaining
        while (++next < this.turns.length) {
            let t = this.turns[next];
            if (t.data.defeated) continue;
            if (t.data.flags.grpga.actions < 1) continue;
            if (t.actor?.effects.find(e => e.getFlag("core", "statusId") === CONFIG.Combat.defeatedStatusId)) continue;
            found = true;
            break;
        }
        if (!found) {// hit the end of the turn order so start from the front
            next = -1;
            while (++next <= turn) {
                let t = this.turns[next];
                if (t.data.defeated) continue;
                if (t.data.flags.grpga.actions < 1) continue;
                if (t.actor?.effects.find(e => e.getFlag("core", "statusId") === CONFIG.Combat.defeatedStatusId)) continue;
                found = true;
                break;
            }
        }

        // Maybe advance to the next round
        let round = this.round;
        if (!found) {
            return this.nextRound();
        }

        // Update the encounter
        const advanceTime = CONFIG.time.turnTime;
        return this.update({ round: round, turn: next }, { advanceTime });
    }

    async nextRound() {

        // fetch everyone's APR
        this.combatants.forEach( c => c.setFlag("grpga", "actions", c.actor.data.data.dynamic.apr?.value));
        for (let t of this.turns) {
            t.data.actions = t.actor.data.data.dynamic.apr?.value;
            await t.setFlag("grpga", "actions", t.data.actions);
        }

        //await this.resetAll();

        return super.nextRound();
    }
}

export class ActionPointCombatTracker extends CombatTracker {
    get template() {
        return "systems/grpga/templates/combat/apcombat-tracker.hbs";
    }

    _onConfigureCombatant(li) {
        const combatant = this.viewed.combatants.get(li.data('combatant-id'));
        new ActionPointCombatantConfig(combatant, {
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400
        }).render(true);
    }

    async getData(options) {
        const data = await super.getData(options);

        if (!data.hasCombat) {
            return data;
        }

        for (let [i, combatant] of data.combat.turns.entries()) {
            data.turns[i].actions = combatant.getFlag("grpga", "actions");
        }
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find(".actions").change(this._onActionsChanged.bind(this));
        html.find(".interrupt").click(this._onInterrupt.bind(this));
        html.find(".initiative").change(this._onInitiativeChanged.bind(this));
    }

    async _onActionsChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        await c.update({ ["flags.grpga.actions"]: btn.value });

        this.render();
    }

    async _onInitiativeChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        await c.update({ initiative: btn.value });

        this.render();
    }

    async _onInterrupt(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        await c.update({ ["flags.grpga.actions"]: c.getFlag("grpga", "actions") - 1 });

        this.render();
    }
}

export class ActionPointCombatantConfig extends CombatantConfig {
    get template() {
        return "systems/grpga/templates/combat/apcombatant-config.hbs";
    }
}

export class ActionPointCombatant extends Combatant {
    _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        this.setFlag("grpga", "actions", this.actor.data.data.dynamic.apr?.value);
    }
}
