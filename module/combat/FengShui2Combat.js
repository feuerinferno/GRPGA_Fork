import { grpga } from "../config.js";

export class FengShui2Combat extends Combat {

    _sortCombatants(a, b) {
        const initA = Number.isNumeric(a.initiative) ? a.initiative : -9999;
        const initB = Number.isNumeric(b.initiative) ? b.initiative : -9999;

        let initDifference = initB - initA;
        if (initDifference != 0) return initDifference;

        const typeA = a.hasPlayerOwner;
        const typeB = b.hasPlayerOwner;

        if (typeA != typeB) {
            if (typeA) return -1;
            if (typeB) return 1;
        }
        return a.tokenId - b.tokenId;
    }

    async _pushHistory(data) {
        let turnHistory = this.getFlag("grpga", "turnHistory").slice();
        turnHistory.push(data);
        return this.setFlag("grpga", "turnHistory", turnHistory);
    }

    async _popHistory() {
        let turnHistory = this.getFlag("grpga", "turnHistory").slice();
        let result = turnHistory.pop();
        await this.setFlag("grpga", "turnHistory", turnHistory);
        return result;
    }

    async spendShots(combatant) {
        this._pushHistory(combatant.getState());
        return combatant.spendShots();
    }

    async rollInitiative(ids, formulaopt, updateTurnopt, messageOptionsopt) {
        await super.rollInitiative(ids, formulaopt, updateTurnopt, messageOptionsopt);
        return this.update({ turn: 0 });
    }

    async startCombat() {
        await this.setupTurns();
        await this.setFlag("grpga", "turnHistory", []);
        return super.startCombat();
    }

    async nextTurn() {
        let missingInitiative = this.combatants.filter(c => c.initiative === null);

        if (missingInitiative.length > 0) {
            missingInitiative.forEach(c => ui.notifications.error(game.i18n.format("grpga.combat.missingInitiative", { token: c.token.name })));
            return this;
        }

        let combatant = this.combatant;

        if (combatant.initiative <= 0) {
            return this.nextRound();
        }

        await this.spendShots(combatant);

        return this.update({ turn: 0 });
    }

    async nextRound() {
        await this._pushHistory(this.combatants.map(c => c.getState()));

        await this._pushHistory("newRound");

        this.combatants.forEach(c => c.setFlag("grpga", "initiativePenalty", Math.min(c.initiative, 0)));

        await this.resetAll();

        return this.update({ round: this.round + 1, turn: 0 }, { advanceTime: CONFIG.time.roundTime });
    }

    async previousRound() {
        const round = Math.max(this.round - 1, 0);

        if (round > 0) {
            let turnHistory = this.getFlag("grpga", "turnHistory").slice();
            let data = turnHistory.pop();

            let roundState;

            if (Array.isArray(data)) {
                roundState = data;
            } else {
                if (data != "newRound") {
                    let index = turnHistory.lastIndexOf("newRound");
                    turnHistory.splice(index);
                }
                roundState = turnHistory.pop();
            }
            await this.setFlag("grpga", "turnHistory", turnHistory);

            for (let c of roundState) {
                const combatant = this.getEmbeddedDocument("Combatant", c.id);
                await combatant.setState(c);
            }

            return this.update({ round: round, turn: 0 }, { advanceTime: -CONFIG.time.roundTime });
        }
    }

    async previousTurn() {
        let data = await this._popHistory();

        if (data == null || data == "newRound") {
            return this.previousRound();
        }

        const combatant = this.getEmbeddedDocument("Combatant", data.id);
        await combatant.setState(data);

        return this.update({ turn: 0 });
    }
}

export class FengShui2CombatTracker extends CombatTracker {
    get template() {
        return "systems/grpga/templates/combat/fs2combat-tracker.hbs";
    }

    _onConfigureCombatant(li) {
        const combatant = this.viewed.combatants.get(li.data('combatant-id'));
        new FengShui2CombatantConfig(combatant, {
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400
        }).render(true);
    }

    async getData(options) {
        const data = await super.getData(options);

        if (!data.hasCombat) {
            return data;
        }

        for (let [i, combatant] of data.combat.turns.entries()) {
            data.turns[i].shotCost = combatant.getFlag("grpga", "shotCost");
        }
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find(".shotCost").change(this._onShotCostChanged.bind(this));
        html.find(".interrupt").click(this._onInterrupt.bind(this));
    }

    async _onShotCostChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        await c.update({ ["flags.grpga.shotCost"]: btn.value });

        this.render();
    }

    async _onInterrupt(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        await combat.spendShots(c);
    }
}

export class FengShui2CombatantConfig extends CombatantConfig {
    get template() {
        return "systems/grpga/templates/combat/fs2combatant-config.hbs";
    }
}

export class FengShui2Combatant extends Combatant {
    _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        this.setFlag("grpga", "shotCost", grpga.combat.defaultShotCost);
    }

    _getInitiativeFormula(combatant) {
        let baseFormula = super._getInitiativeFormula(combatant);
        const initiativePenalty = this.getFlag("grpga", "initiativePenalty");

        if (initiativePenalty < 0) {
            baseFormula += ` + ${initiativePenalty}`;
        }
        return baseFormula;
    }

    async spendShots() {
        const shotsSpent = this.getFlag("grpga", "shotCost");
        const newInitiative = this.initiative - shotsSpent;

        return this.update({
            initiative: newInitiative,
            ["flags.grpga.shotCost"]: grpga.combat.defaultShotCost,
            ["flags.grpga.initiativePenalty"]: Math.min(newInitiative, 0)
        });
    }

    async setState(data) {
        return this.update({
            initiative: data.initiative,
            ["flags.grpga.shotCost"]: data.shotCost
        });
    }

    getState() {
        return {
            id: this.id,
            initiative: this.initiative,
            shotCost: this.getFlag("grpga", "shotCost")
        };
    };
}
