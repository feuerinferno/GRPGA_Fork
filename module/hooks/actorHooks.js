import { countRanks, resetRanks } from "../actor/baseActor.js";

/**
* Force ranks counting before rendering
*/
Hooks.on("getActorVsDSheetHeaderButtons", (sheet, buttonarray) => {
  if (CONFIG.grpga.testMode) { console.debug("getActorVsDSheetHeaderButtons\n", [sheet, buttonarray]); }
  return;
  // when I remember why we aren't doing this anymore I will note it here :-(
  sheet.actor.data.flags.world.countRanks = true;
  sheet.actor.data.flags.world.resetRanks = true;
});

/**
* Organise some data before rendering the sheet
*/
Hooks.on("renderActorVsDSheet", (sheet, html, data) => {
  const actor = sheet.actor;
  if (CONFIG.grpga.testMode) { console.debug("renderActorVsDSheet\n", [sheet, html, data, actor.data.data]); }
  return;
  // when I remember why we aren't doing this anymore I will note it here :-(
    if (actor.getFlag("world", "countRanks")) {
    //    actor.data.flags.world.countRanks = false;
    countRanks(actor);
  }
  if (actor.getFlag("world", "resetRanks")) {
    resetRanks(actor);
  }
});

/**
 * Specific Tailoring of actor types on creation
 * 
 * Preparing to fake actor subclasses and need to make all actor templates the same
 */
Hooks.on('preCreateActor', (document, data, options, userId) => {
  if (CONFIG.grpga.testMode) console.debug("preCreateActor:\n", [document, data, options, userId]);
  // if the actor has an image, it already exists. No reason to be doing anything to it here yet.
  if (data.img) return;

  const newdata = {};

  // set the basic features according to system requirements
  switch (data.type) {
    case "CharacterD20":
    case "CharacterD20Custom":
    {
      newdata.bm = {
        step: 30
      }
      break;
    }
    case "Character3D6": {
      newdata.bm = {
        step: 1,
        quarter: 1,
        half: 2,
        move: 5,
        sprint: 6
      }
      break;
    }
    case "CharacterD120": {
      newdata.bm = {
        step: 30
      }
      break;
    }
    case "CharacterGRP": {
      newdata.bm = {
        step: 30
      }
      break;
    }
    case "CharacterOaTS": {
      newdata.bm = {
        step: 0
      }
      break;
    }
    case "CharacterD100": {
      newdata.bm = {
        step: 50
      }
      break;
    }
    case "CharacterVsD": {
      newdata.bm = {
        step: 15
      }
      break;
    }
  }
  data.data = newdata;
  document.data.update(data);
});

