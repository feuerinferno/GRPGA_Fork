Hooks.on('createItem', (document, options, userId) => {
  if (CONFIG.grpga.testMode) console.debug("createItem:\n", [document, options, userId]);

  // items dragged from the sidebar will lose their moddedformula if they have one so calculate it again
  switch (document.type) {
    case "Melee-Attack":
    case "Ranged-Attack":
    case "Defence":
    case "Rollable": {
      let docdata = document.data.data;
      if (CONFIG.grpga.testMode) console.debug("Item Data:\n", docdata);
      if (docdata.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = docdata.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          docdata.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          docdata.value = 0;
          docdata.moddedvalue = 0;
          docdata.moddedformula = formula;
        }
      }
      break;
    }
  }
});

Hooks.on('preCreateItem', (document, data, options, userId) => {
  if (CONFIG.grpga.testMode) console.debug("preCreateItem:\n", [document, data, options, userId]);

  if (document.data._id) {// This item exists already but may have the wrong chartype
    if (document.actor) {
      // if there is an actor and the chartype matches then exit
      if (document.actor.type == data.data.chartype) return;
      data.data.chartype = document.actor.type;
    } else {
      // if the chartype matches the ruleset then exit
      if (CONFIG.grpga.chartype == data.data.chartype) return;
      data.data.chartype = CONFIG.grpga.chartype;
    }
    // correct the chartype then exit
    document.data.update(data);
    return;
  } else if (data.data == undefined) {
    // the item is being created from the item sidebar so give it a chartype
    data.data = document.data.data;
    data.data.chartype = CONFIG.grpga.chartype;
  }

  if (!data.data.group) { // initialise group field to match item category or type
    data.data.group = data.data.category || data.type;
  }

  switch (data.data.chartype) {
    case "CharacterD100": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          data.data.armourDiv = 5;
          data.data.damage = "";
          data.data.damageType = "";
          data.data.minST = "oeh";
          data.data.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          data.data.reach = 5;
          data.data.weight = 100;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          data.data.armourDiv = 5;
          data.data.damage = "";
          data.data.damageType = "";
          data.data.minST = "oeh";
          data.data.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          data.data.rof = 5;
          data.data.accuracy = 100;
          data.data.range = "10";
          break;
        }
        case "Hit-Location": { // Wound
          data.img = "icons/svg/blood.svg";
          data.name = "Wound";
          break;
        }
        case "Defence": { // dodge(std), parry(oeh), block(oe)
          data.img = "icons/svg/mage-shield.svg";
          data.name = "Various";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    default: {
      data.img = "icons/svg/mystery-man-black.svg";
      break;
    }
  }
  document.data.update(data);
});

Hooks.on('preUpdateItem', (document, change, options, userId) => {
  if (CONFIG.grpga.testMode) console.debug("preUpdateItem:\n", [document, change, options, userId]);

  switch (document.type) {
    case "Modifier": {
      if (CONFIG.grpga.testMode) console.debug("Modifier:\n", change.data);

      // refresh all the categories in case one has changed
      document.data.data = mergeObject(document.data.data, change.data);
      let newdata = {
        attack: false,
        damage: false,
        defence: false,
        reaction: false,
        skill: false,
        spell: false,
        check: false,
        primary: false,
        entries: {
          0: { value: 0 }
        }
      };
      let entries = Object.values(document.data.data.entries);
      for (let i = 1; i < entries.length; i++) { //ignore category changes to [0]
        switch (entries[i].category) {
          case "attack": newdata.attack = true; break;
          case "damage": newdata.damage = true; break;
          case "defence": newdata.defence = true; break;
          case "reaction": newdata.reaction = true; break;
          case "skill": newdata.skill = true; break;
          case "spell": newdata.spell = true; break;
          case "check": newdata.check = true; break;
          case "primary": newdata.primary = true; break;
        }
      }
      change.data = mergeObject(newdata, change.data);
      break;
    }
    case "Variable": {
      if (CONFIG.grpga.testMode) console.debug("Variable:\n", change.data);

      if (change.data?.label) { // we have selected a new value
        let entries = Object.values(document.data.data.entries);
        for (let i = 1; i < entries.length; i++) { //ignore category changes to [0]
          if (entries[i].label == change.data.label) { // we have found the entry for the new label
            if (Number.isNumeric(entries[i].formula)) { // if it is a number
              entries[i].value = Number(entries[i].formula); // recalculate the value
              change.data.entries = entries;
              change.data.entries[i] = { value: entries[i].value, formula: entries[i].formula, label: entries[i].label }; // add the entry to the changed data
            }
            // reassign the updated document formula and value in case they changed
            change.data.value = entries[i].value;
            change.data.formula = entries[i].formula;
            break;
          }
        }
      } else if (change.data?.entries) { // we edited an entry
        let entry = Object.entries(change.data.entries)[0];
        if (entry[1].formula) {// only interested if the formula was changed
          change.data.entries[entry[0]].value = Number(change.data.entries[entry[0]].formula); // add the new value to the changed data
          if (document.data.data.entries[entry[0]].label == document.data.data.label) { // the entry changed was the one currently selected
            // reassign the updated document formula and value
            change.data.formula = change.data.entries[entry[0]].formula;
            change.data.value = change.data.entries[entry[0]].value;
          }
        }
      }
      break;
    }
    case "Melee-Attack":
    case "Ranged-Attack":
    case "Defence":
    case "Rollable": {
      if (CONFIG.grpga.testMode) console.debug("Formula Carrier:\n", change.data);
      if (change.data?.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = change.data.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          change.data.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          change.data.value = 0;
          change.data.moddedvalue = 0;
          change.data.moddedformula = formula;
        }
      }
      break;
    }
  }
});
