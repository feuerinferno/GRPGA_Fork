let targets = Object.values(window.NPCTargeting.tt.records.items);
let content = "<table><tr><th>Source</th><th>Target</th></tr>";
for (let t of targets) {
    if (t.sourceID == t.targetID) continue;
    let src = canvas.tokens.get(t.sourceID).name;
    let tgt = canvas.tokens.get(t.targetID).name;
    content += `<tr><td>${src}</td><td>${tgt}</td><tr>`;
}
content += "</table>"

new Dialog({
    title: "Current Targets",
    content: content,
    buttons: {
        close: {
            icon: "<i class='fas fa-tick'></i>",
            label: "Close"
        },
    },
    default: "close"
}).render(true)