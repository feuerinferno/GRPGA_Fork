# grpga

Foundry Virtual TableTop support for a Generic RolePlaying Game Aid.

The short-term goal of GRPGA is to support play of a D20 system, a 3D6 system and, possibly, a D100 system, all within the same framework.

A long-term goal is to capture the advantages of each system and create my own based on timeline tactical activity instead of turn-based action.

If you would like to support this effort, you may do so here; https://www.patreon.com/jbhuddleston

To join the discussion or monitor progress on Discord, join here; https://discord.gg/d8ujbNG
